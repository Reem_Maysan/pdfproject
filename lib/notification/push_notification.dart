import 'dart:async';
import 'dart:io';

//import 'package:device_info/device_info.dart';
import 'package:dio/dio.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pdfproject/constents/constent.dart';

class PushNotificationManager {
  PushNotificationManager._();

  factory PushNotificationManager() => _instance;

  static final PushNotificationManager _instance = PushNotificationManager._();

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();


//  NotificationManager notificationManager;
  bool _initialized = false;

  /* Future<String> init(BuildContext context) async {
    String token;

    if (!_initialized) {
      // For iOS request permission first.
      if (Platform.isIOS) {
        _firebaseMessaging
            .requestNotificationPermissions(IosNotificationSettings());
      }

//      notificationManager = new NotificationManager();
//      notificationManager.initNotificationManager();

      _firebaseMessaging.configure(
          onMessage: (Map<String, dynamic> message) async {
        print('onMessage: ' + message.toString());
//        _showDialog(context, message);
      }, onLaunch: (Map<String, dynamic> message) async {
        print('onLaunch: ' + message.toString());
        navigate(context, message);
      }, onResume: (Map<String, dynamic> message) async {
        print('onResume: ' + message.toString());
        navigate(context, message);
      });

      // For testing purposes print the Firebase Messaging token
      token = await _firebaseMessaging.getToken();
      print("FirebaseMessaging token: $token");

      _initialized = true;
    }

    return token;
  }
*/
  navigate(BuildContext context, Map<String, dynamic> message) {
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  Future<String> init(BuildContext context) async {
    String token;
    // this.context = context;

    if (!_initialized) {
      // For iOS request permission first.
      if (Platform.isIOS) {
        _firebaseMessaging.requestNotificationPermissions(
            IosNotificationSettings(
                sound: true, badge: true, alert: true, provisional: false));
      }

      _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async {
          print('onMessage: ' + message.toString());
          // _showDialog(context, message);
        },
        onLaunch: (Map<String, dynamic> message) async {
          print('onLaunch: ' + message.toString());
          navigate(context, message);
        },
        onResume: (Map<String, dynamic> message) async {
          print('onResume: ' + message.toString());
          navigate(context, message);
        },
      );

      token = await _firebaseMessaging.getToken();
      print("FirebaseMessaging token: $token");
      sendFCMToken(token);

      Stream<String> fcm = _firebaseMessaging.onTokenRefresh;
      fcm.listen((token) {
        print('refreshToken: ' + token);
        sendFCMToken(token);
      });
      _initialized = true;
    }

    return token;
  }

// add this func to push_notitication class

  sendFCMToken(String fcmToken) async {

    Dio dio=new Dio();
    try {

      await dio
          .post(BaseUrl + 'updateFcmToken',
              queryParameters: {'fcm_token': fcmToken},
              options: Options(
                  contentType: Headers.formUrlEncodedContentType,
                  headers: {
                    'Accept': 'application/json',
                  }))
          .then((res) {
        print('FCM token updated');
      });
    } on DioError catch (e) {}
  }

//  Future<void> _showDialog(
//      BuildContext context, Map<String, dynamic> message) async {
//    print(message.toString());
//
//    var fetchedMessage;
//    Map<String, dynamic> alternativeMessage = {
//      'titleEn': 'new forsa for you',
//      'title': 'فرصة جديدة لك',
//      'bodyEn': 'new forsa for you',
//      'body': 'فرصة جديدة لك',
//    };
////    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
//
////    if (Platform.isIOS) {
////      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
////      print('ios = ${iosInfo.identifierForVendor}');
////      if (Platform.isIOS &&
////          int.parse(iosInfo.systemVersion.split('.')[0]) < 13) {
////        fetchedMessage = message['aps']['alert'];
////      } else {
////        if (message['data'] != null)
////          fetchedMessage = message['data'];
////        else if (message['data'] == null && message['notification'] != null)
////          fetchedMessage = message['notification'];
////        else
////          fetchedMessage = alternativeMessage;
////      }
////    } else {
//    if (message['data'] != null)
//      fetchedMessage = message['data'];
//    else if (message['data'] == null && message['notification'] != null)
//      fetchedMessage = message['notification'];
//    else
//      fetchedMessage = alternativeMessage;
////    }
//
//    if (fetchedMessage != null) {
//      if (Utils.getInstance(context).getLanguage(context) == 'en') {
//        if (fetchedMessage['titleEn'] != null)
//          notificationManager.showNotificationWithDefaultSound(
//              fetchedMessage['titleEn'], fetchedMessage['bodyEn']);
//        else
//          notificationManager.showNotificationWithDefaultSound(
//              fetchedMessage['title'], fetchedMessage['body']);
//      } else {
//        notificationManager.showNotificationWithDefaultSound(
//            fetchedMessage['title'], fetchedMessage['body']);
//      }
//
//      showOverlayNotification((context) {
//        return Card(
//          margin: const EdgeInsets.symmetric(horizontal: 4),
//          child: SafeArea(
//            child: ListTile(
//              leading: SizedBox.fromSize(
//                  size: const Size(45, 45),
//                  child: ClipOval(
//                      child: Container(
//                    decoration: new BoxDecoration(
//                      color: Colors.transparent,
//                      image: DecorationImage(
//                        image: new AssetImage('assets/images/app_ic.png'),
//                        fit: BoxFit.fill,
//                      ),
//                      shape: BoxShape.circle,
//                    ),
//                  ))),
//              title: Text(
//                  (Utils.getInstance(context).getLanguage(context) == 'en' &&
//                          fetchedMessage['titleEn'] != null)
//                      ? fetchedMessage['titleEn']
//                      : fetchedMessage['title']),
//              subtitle: Text(
//                  (Utils.getInstance(context).getLanguage(context) == 'en' &&
//                      fetchedMessage['bodyEn'] != null)
//                      ? fetchedMessage['bodyEn']
//                      : fetchedMessage['body']),
//            ),
//          ),
//        );
//      }, duration: Duration(milliseconds: 4000));
//    }
//  }
}

//class NotificationManager {
//  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
//  AndroidInitializationSettings initializationSettingsAndroid;
//  IOSInitializationSettings initializationSettingsIOS;
//  InitializationSettings initializationSettings;
//
//  void initNotificationManager() {
//    initializationSettingsAndroid =
//        new AndroidInitializationSettings('@mipmap/ic_launcher');
//    initializationSettingsIOS = new IOSInitializationSettings();
//    initializationSettings = new InitializationSettings(
//        initializationSettingsAndroid, initializationSettingsIOS);
//    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
//    flutterLocalNotificationsPlugin.initialize(initializationSettings);
//  }
//
//  void showNotificationWithDefaultSound(String title, String body) {
//    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
//        'your channel id', 'your channel name', 'your channel description',
//        importance: Importance.Max, priority: Priority.High);
//    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
//    var platformChannelSpecifics = new NotificationDetails(
//        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
//    flutterLocalNotificationsPlugin.show(
//        0, title, body, platformChannelSpecifics);
//  }
//}
