import 'package:flutter/material.dart';

// Urls
const String BaseUrl = "https://athaqafia.org/api/";

const String LaunchedUrl = "https://athaqafia.org/";

const String ImageUrl = "https://athaqafia.org/images/";

const String PdfUrl = "https://athaqafia.org/files/";

// Colors
const Color BlackColor = Color(0xff363638);

const Color BackGroundColor = Color(0xffEFEDED);

const Color AmberColor = Color(0xffFFC019);

const Color GrayColor = Color(0xffAFAFAF);

// images

const String PressedBookButton = 'assets/Icons/s_read.png';
const String UnPressedBookButton = 'assets/Icons/uns_read.png';

const String PressedResearchButton = 'assets/Icons/s_research.png';
const String UnPressedResearchButton = 'assets/Icons/uns_research.png';

const String PressedPublicationButton = 'assets/Icons/s_publication.png';
const String UnPressedPublicationButton = 'assets/Icons/uns_publication.png';

const String DownloadButton = 'assets/Icons/downloadBut.png';
const String ViewButton = 'assets/Icons/viewBut.png';

// Sizes
const double AppBarTitleSize = 30;

const double DescriptionSize = 30;

const double ItemTitleSize = 15;

const double textSize = 35;

const double radius = 30;

// messages

const String isReadyPdfPref = 'isReadyPdf';
const String ErrorMessage = "حدث خطأ ما !";



final colors = [
  Color(0xf3e2c1),
  Color(0xFFC03F),
  Color(0xFFC03F),
  Color(0xDFC244),
  Color(0xFFC03F),
];

class PageParameters {
  final int pageCount;
  final int pageStartId;

  PageParameters({this.pageCount, this.pageStartId});

  int getPageCount() {
    return this.pageCount;
  }

  int getPageStartId() {
    return this.pageStartId;
  }
}
