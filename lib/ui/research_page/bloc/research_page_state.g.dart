// GENERATED CODE - DO NOT MODIFY BY HAND

part of research_page_state;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ResearchState extends ResearchState {
  @override
  final String error;
  @override
  final bool isLoading;
  @override
  final bool isLoadingPdf;
  @override
  final ResearchModel researches;
  @override
  final int count;
  @override
  final int start_id;
  @override
  final bool isEmptyList;
  @override
  final bool isExpanded;
  @override
  final bool loadMoreRequest;

  factory _$ResearchState([void Function(ResearchStateBuilder) updates]) =>
      (new ResearchStateBuilder()..update(updates)).build();

  _$ResearchState._(
      {this.error,
      this.isLoading,
      this.isLoadingPdf,
      this.researches,
      this.count,
      this.start_id,
      this.isEmptyList,
      this.isExpanded,
      this.loadMoreRequest})
      : super._() {
    if (error == null) {
      throw new BuiltValueNullFieldError('ResearchState', 'error');
    }
    if (isLoading == null) {
      throw new BuiltValueNullFieldError('ResearchState', 'isLoading');
    }
    if (isLoadingPdf == null) {
      throw new BuiltValueNullFieldError('ResearchState', 'isLoadingPdf');
    }
    if (count == null) {
      throw new BuiltValueNullFieldError('ResearchState', 'count');
    }
    if (start_id == null) {
      throw new BuiltValueNullFieldError('ResearchState', 'start_id');
    }
    if (isEmptyList == null) {
      throw new BuiltValueNullFieldError('ResearchState', 'isEmptyList');
    }
    if (isExpanded == null) {
      throw new BuiltValueNullFieldError('ResearchState', 'isExpanded');
    }
    if (loadMoreRequest == null) {
      throw new BuiltValueNullFieldError('ResearchState', 'loadMoreRequest');
    }
  }

  @override
  ResearchState rebuild(void Function(ResearchStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ResearchStateBuilder toBuilder() => new ResearchStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ResearchState &&
        error == other.error &&
        isLoading == other.isLoading &&
        isLoadingPdf == other.isLoadingPdf &&
        researches == other.researches &&
        count == other.count &&
        start_id == other.start_id &&
        isEmptyList == other.isEmptyList &&
        isExpanded == other.isExpanded &&
        loadMoreRequest == other.loadMoreRequest;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc($jc($jc(0, error.hashCode), isLoading.hashCode),
                                isLoadingPdf.hashCode),
                            researches.hashCode),
                        count.hashCode),
                    start_id.hashCode),
                isEmptyList.hashCode),
            isExpanded.hashCode),
        loadMoreRequest.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ResearchState')
          ..add('error', error)
          ..add('isLoading', isLoading)
          ..add('isLoadingPdf', isLoadingPdf)
          ..add('researches', researches)
          ..add('count', count)
          ..add('start_id', start_id)
          ..add('isEmptyList', isEmptyList)
          ..add('isExpanded', isExpanded)
          ..add('loadMoreRequest', loadMoreRequest))
        .toString();
  }
}

class ResearchStateBuilder
    implements Builder<ResearchState, ResearchStateBuilder> {
  _$ResearchState _$v;

  String _error;
  String get error => _$this._error;
  set error(String error) => _$this._error = error;

  bool _isLoading;
  bool get isLoading => _$this._isLoading;
  set isLoading(bool isLoading) => _$this._isLoading = isLoading;

  bool _isLoadingPdf;
  bool get isLoadingPdf => _$this._isLoadingPdf;
  set isLoadingPdf(bool isLoadingPdf) => _$this._isLoadingPdf = isLoadingPdf;

  ResearchModelBuilder _researches;
  ResearchModelBuilder get researches =>
      _$this._researches ??= new ResearchModelBuilder();
  set researches(ResearchModelBuilder researches) =>
      _$this._researches = researches;

  int _count;
  int get count => _$this._count;
  set count(int count) => _$this._count = count;

  int _start_id;
  int get start_id => _$this._start_id;
  set start_id(int start_id) => _$this._start_id = start_id;

  bool _isEmptyList;
  bool get isEmptyList => _$this._isEmptyList;
  set isEmptyList(bool isEmptyList) => _$this._isEmptyList = isEmptyList;

  bool _isExpanded;
  bool get isExpanded => _$this._isExpanded;
  set isExpanded(bool isExpanded) => _$this._isExpanded = isExpanded;

  bool _loadMoreRequest;
  bool get loadMoreRequest => _$this._loadMoreRequest;
  set loadMoreRequest(bool loadMoreRequest) =>
      _$this._loadMoreRequest = loadMoreRequest;

  ResearchStateBuilder();

  ResearchStateBuilder get _$this {
    if (_$v != null) {
      _error = _$v.error;
      _isLoading = _$v.isLoading;
      _isLoadingPdf = _$v.isLoadingPdf;
      _researches = _$v.researches?.toBuilder();
      _count = _$v.count;
      _start_id = _$v.start_id;
      _isEmptyList = _$v.isEmptyList;
      _isExpanded = _$v.isExpanded;
      _loadMoreRequest = _$v.loadMoreRequest;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ResearchState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ResearchState;
  }

  @override
  void update(void Function(ResearchStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ResearchState build() {
    _$ResearchState _$result;
    try {
      _$result = _$v ??
          new _$ResearchState._(
              error: error,
              isLoading: isLoading,
              isLoadingPdf: isLoadingPdf,
              researches: _researches?.build(),
              count: count,
              start_id: start_id,
              isEmptyList: isEmptyList,
              isExpanded: isExpanded,
              loadMoreRequest: loadMoreRequest);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'researches';
        _researches?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ResearchState', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
