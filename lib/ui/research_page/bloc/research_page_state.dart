library research_page_state;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:pdfproject/models/research_model/research_model.dart';

part 'research_page_state.g.dart';

abstract class ResearchState
    implements Built<ResearchState, ResearchStateBuilder> {
  // fields go here

  ResearchState._();

  String get error;

  bool get isLoading;

  bool get isLoadingPdf;

  @nullable
  ResearchModel get researches;

  int get count;

  int get start_id;

  bool get isEmptyList;

  bool get isExpanded;

  bool get loadMoreRequest;
  factory ResearchState([updates(ResearchStateBuilder b)]) = _$ResearchState;

  factory ResearchState.initail() {
    return ResearchState((b) => b
      ..error = ""
      ..isLoading = false
      ..researches = null
      ..count = 2
      ..start_id = 0
      ..isEmptyList = false
      ..isExpanded= false
      ..loadMoreRequest = false
      ..isLoadingPdf=false);
  }
}
