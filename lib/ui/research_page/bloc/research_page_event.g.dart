// GENERATED CODE - DO NOT MODIFY BY HAND

part of research_page_event;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ClearError extends ClearError {
  factory _$ClearError([void Function(ClearErrorBuilder) updates]) =>
      (new ClearErrorBuilder()..update(updates)).build();

  _$ClearError._() : super._();

  @override
  ClearError rebuild(void Function(ClearErrorBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ClearErrorBuilder toBuilder() => new ClearErrorBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ClearError;
  }

  @override
  int get hashCode {
    return 507656265;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('ClearError').toString();
  }
}

class ClearErrorBuilder implements Builder<ClearError, ClearErrorBuilder> {
  _$ClearError _$v;

  ClearErrorBuilder();

  @override
  void replace(ClearError other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ClearError;
  }

  @override
  void update(void Function(ClearErrorBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ClearError build() {
    final _$result = _$v ?? new _$ClearError._();
    replace(_$result);
    return _$result;
  }
}

class _$GetResearches extends GetResearches {
  @override
  final int count;
  @override
  final int start_id;
  @override
  final bool showProgress;
  @override
  final bool loadMoreRequest;

  factory _$GetResearches([void Function(GetResearchesBuilder) updates]) =>
      (new GetResearchesBuilder()..update(updates)).build();

  _$GetResearches._(
      {this.count, this.start_id, this.showProgress, this.loadMoreRequest})
      : super._() {
    if (count == null) {
      throw new BuiltValueNullFieldError('GetResearches', 'count');
    }
    if (start_id == null) {
      throw new BuiltValueNullFieldError('GetResearches', 'start_id');
    }
    if (showProgress == null) {
      throw new BuiltValueNullFieldError('GetResearches', 'showProgress');
    }
    if (loadMoreRequest == null) {
      throw new BuiltValueNullFieldError('GetResearches', 'loadMoreRequest');
    }
  }

  @override
  GetResearches rebuild(void Function(GetResearchesBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetResearchesBuilder toBuilder() => new GetResearchesBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetResearches &&
        count == other.count &&
        start_id == other.start_id &&
        showProgress == other.showProgress &&
        loadMoreRequest == other.loadMoreRequest;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, count.hashCode), start_id.hashCode),
            showProgress.hashCode),
        loadMoreRequest.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GetResearches')
          ..add('count', count)
          ..add('start_id', start_id)
          ..add('showProgress', showProgress)
          ..add('loadMoreRequest', loadMoreRequest))
        .toString();
  }
}

class GetResearchesBuilder
    implements Builder<GetResearches, GetResearchesBuilder> {
  _$GetResearches _$v;

  int _count;
  int get count => _$this._count;
  set count(int count) => _$this._count = count;

  int _start_id;
  int get start_id => _$this._start_id;
  set start_id(int start_id) => _$this._start_id = start_id;

  bool _showProgress;
  bool get showProgress => _$this._showProgress;
  set showProgress(bool showProgress) => _$this._showProgress = showProgress;

  bool _loadMoreRequest;
  bool get loadMoreRequest => _$this._loadMoreRequest;
  set loadMoreRequest(bool loadMoreRequest) =>
      _$this._loadMoreRequest = loadMoreRequest;

  GetResearchesBuilder();

  GetResearchesBuilder get _$this {
    if (_$v != null) {
      _count = _$v.count;
      _start_id = _$v.start_id;
      _showProgress = _$v.showProgress;
      _loadMoreRequest = _$v.loadMoreRequest;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetResearches other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetResearches;
  }

  @override
  void update(void Function(GetResearchesBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetResearches build() {
    final _$result = _$v ??
        new _$GetResearches._(
            count: count,
            start_id: start_id,
            showProgress: showProgress,
            loadMoreRequest: loadMoreRequest);
    replace(_$result);
    return _$result;
  }
}

class _$GetResearchIcon extends GetResearchIcon {
  @override
  final bool isExpandedIcon;
  @override
  final int indexIcon;

  factory _$GetResearchIcon([void Function(GetResearchIconBuilder) updates]) =>
      (new GetResearchIconBuilder()..update(updates)).build();

  _$GetResearchIcon._({this.isExpandedIcon, this.indexIcon}) : super._() {
    if (isExpandedIcon == null) {
      throw new BuiltValueNullFieldError('GetResearchIcon', 'isExpandedIcon');
    }
    if (indexIcon == null) {
      throw new BuiltValueNullFieldError('GetResearchIcon', 'indexIcon');
    }
  }

  @override
  GetResearchIcon rebuild(void Function(GetResearchIconBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetResearchIconBuilder toBuilder() =>
      new GetResearchIconBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetResearchIcon &&
        isExpandedIcon == other.isExpandedIcon &&
        indexIcon == other.indexIcon;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, isExpandedIcon.hashCode), indexIcon.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GetResearchIcon')
          ..add('isExpandedIcon', isExpandedIcon)
          ..add('indexIcon', indexIcon))
        .toString();
  }
}

class GetResearchIconBuilder
    implements Builder<GetResearchIcon, GetResearchIconBuilder> {
  _$GetResearchIcon _$v;

  bool _isExpandedIcon;
  bool get isExpandedIcon => _$this._isExpandedIcon;
  set isExpandedIcon(bool isExpandedIcon) =>
      _$this._isExpandedIcon = isExpandedIcon;

  int _indexIcon;
  int get indexIcon => _$this._indexIcon;
  set indexIcon(int indexIcon) => _$this._indexIcon = indexIcon;

  GetResearchIconBuilder();

  GetResearchIconBuilder get _$this {
    if (_$v != null) {
      _isExpandedIcon = _$v.isExpandedIcon;
      _indexIcon = _$v.indexIcon;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetResearchIcon other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetResearchIcon;
  }

  @override
  void update(void Function(GetResearchIconBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetResearchIcon build() {
    final _$result = _$v ??
        new _$GetResearchIcon._(
            isExpandedIcon: isExpandedIcon, indexIcon: indexIcon);
    replace(_$result);
    return _$result;
  }
}

class _$GetPdf extends GetPdf {
  @override
  final String pdfUrl;

  factory _$GetPdf([void Function(GetPdfBuilder) updates]) =>
      (new GetPdfBuilder()..update(updates)).build();

  _$GetPdf._({this.pdfUrl}) : super._() {
    if (pdfUrl == null) {
      throw new BuiltValueNullFieldError('GetPdf', 'pdfUrl');
    }
  }

  @override
  GetPdf rebuild(void Function(GetPdfBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetPdfBuilder toBuilder() => new GetPdfBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetPdf && pdfUrl == other.pdfUrl;
  }

  @override
  int get hashCode {
    return $jf($jc(0, pdfUrl.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GetPdf')..add('pdfUrl', pdfUrl))
        .toString();
  }
}

class GetPdfBuilder implements Builder<GetPdf, GetPdfBuilder> {
  _$GetPdf _$v;

  String _pdfUrl;
  String get pdfUrl => _$this._pdfUrl;
  set pdfUrl(String pdfUrl) => _$this._pdfUrl = pdfUrl;

  GetPdfBuilder();

  GetPdfBuilder get _$this {
    if (_$v != null) {
      _pdfUrl = _$v.pdfUrl;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetPdf other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetPdf;
  }

  @override
  void update(void Function(GetPdfBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetPdf build() {
    final _$result = _$v ?? new _$GetPdf._(pdfUrl: pdfUrl);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
