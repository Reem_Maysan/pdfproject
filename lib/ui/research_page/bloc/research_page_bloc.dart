import 'dart:io';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pdfproject/data/repository/irepository.dart';
import 'package:pdfproject/ui/research_page/bloc/research_page_event.dart';
import 'package:pdfproject/ui/research_page/bloc/research_page_state.dart';

class ResearchBloc extends Bloc<ResearchEvent, ResearchState> {
  IRepository _repository;

  ResearchBloc(this._repository);

  @override
  ResearchState get initialState => ResearchState.initail();

  @override
  Stream<ResearchState> mapEventToState(ResearchEvent event) async* {
    if (event is ClearError) {
      yield state.rebuild((b) => b..error = "");
    }
    if (event is GetResearches && !state.isEmptyList) {
      try {
        yield state.rebuild((b) => b
          ..isLoading = event.showProgress
          ..error = "");
        final data =
            await _repository.getResearches(event.start_id, event.count);

        yield data.researches.isEmpty
            ? state.rebuild((b) => b..isEmptyList = true)
            : state.rebuild((b) => b
              ..isLoading = false
              ..error = ""
              ..count = event.count
              ..start_id = event.start_id
              ..loadMoreRequest = false
              ..isEmptyList = data.researches.isEmpty
              ..researches.update((b) {
                if (!state.isEmptyList) {
                  b..researches.addAll(data.researches);
                  b
                    ..researches
                        .map((f) => f.rebuild((b) => b..isExpanded = false));
                }
              }));
      } catch (e) {
        print('GetResearches Error $e');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = "Something went wrong");
      }
    }
    if (event is GetPdf) {
      try {
        yield state.rebuild((b) => b..isLoadingPdf = true);
        final data = await _repository.getPdf(event.pdfUrl);
        yield state.rebuild((b) => b..isLoadingPdf = false);
        print('GetPdf response $data');
      } catch (e) {
        print('GetPdf Error $e');
        yield state.rebuild((b) => b..error = "Something went wrong");
      }
    }
    if (event is GetResearchIcon) {
      try {
        yield state.rebuild((b) => b
          ..researches.update((b) {
            if (!state.isEmptyList) {
              b
                ..researches.map((f) {
                  if (f.id == event.indexIcon) {
                    print('weeee fouuund it!! its: ' +
                        f.id.toString() +
                        'b..isExpanded : ' +
                        f.isExpanded.toString() +
                        ' become :' +
                        event.isExpandedIcon.toString());
                    return f
                        .rebuild((b) => b..isExpanded = event.isExpandedIcon);
                  } else
                    return f.rebuild((b) => b..isExpanded = b.isExpanded);
                });
            }
          }));
      } catch (e) {
        print('GetResearchIcon Error $e');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = "Something went wrong");
      }
    }

  }
}
