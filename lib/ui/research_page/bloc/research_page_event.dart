library research_page_event;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'research_page_event.g.dart';

abstract class ResearchEvent {}

abstract class ClearError extends ResearchEvent
    implements Built<ClearError, ClearErrorBuilder> {
  // fields go here

  ClearError._();

  factory ClearError([updates(ClearErrorBuilder b)]) = _$ClearError;
}

abstract class GetResearches extends ResearchEvent
    implements Built<GetResearches, GetResearchesBuilder> {
  int get count;

  int get start_id;

  bool get showProgress;

  bool get loadMoreRequest;
  GetResearches._();

  factory GetResearches([updates(GetResearchesBuilder b)]) = _$GetResearches;
}


abstract class GetResearchIcon extends ResearchEvent
    implements Built<GetResearchIcon, GetResearchIconBuilder> {
  // fields go here

  bool get isExpandedIcon;
  int get indexIcon;

  GetResearchIcon._();

  factory GetResearchIcon([updates(GetResearchIconBuilder b)]) = _$GetResearchIcon;
}



abstract class GetPdf extends ResearchEvent
    implements Built<GetPdf, GetPdfBuilder> {
  // fields go here

  String get pdfUrl;

  GetPdf._();

  factory GetPdf([updates(GetPdfBuilder b)]) = _$GetPdf;
}
