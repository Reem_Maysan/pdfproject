import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pdfproject/constents/constent.dart';
import 'package:pdfproject/injectoin.dart';
import 'package:pdfproject/models/research_model/research_list.dart';
import 'package:pdfproject/ui/research_page/bloc/research_page_bloc.dart';
import 'package:pdfproject/ui/research_page/bloc/research_page_event.dart';
import 'package:pdfproject/ui/research_page/bloc/research_page_state.dart';

class ResearchesAndStudiesPage extends StatefulWidget {
  @override
  _ResearchesAndStudiesPageState createState() =>
      _ResearchesAndStudiesPageState();
}

class _ResearchesAndStudiesPageState extends State<ResearchesAndStudiesPage> {
  PDFDocument _pdfDocument;
  bool _isReadyPdf = false;

  final _bloc = sl<ResearchBloc>();
  ResearchState state;
  bool showProgressLoadMore = true;
  final _scrollController = ScrollController();
  final _scrollThreshold = 200.0;
  bool loadMoreRequest = false;

  DateTime currentBackPressTime;

  void _loadPdf(String url) async {
    print('fffffffffffffffffffffffffffffffff');
    setState(() {
      _isReadyPdf = false;
    });
    final pdf = await PDFDocument.fromURL(url);
    setState(() {
      _pdfDocument = pdf;
      _isReadyPdf = true;
    });
  }

  @override
  void initState() {
    _scrollController.addListener(_onScroll);
    _bloc.add(GetResearches((b) => b
      ..start_id = 0
      ..count = 10
      ..loadMoreRequest = false
      ..showProgress = true));
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll <= _scrollThreshold && !loadMoreRequest) {
      _loadMore();
    }
  }

  Future<bool> _loadMore() async {
    loadMoreRequest = true;
    print('-loadMore');
    if (state.researches != null && (state.isLoading || state.isEmptyList)) {
      return false;
    }
    if (state.researches != null) {
      _bloc.add(GetResearches((b) => b
        ..start_id = state.researches.researches
            .elementAt(state.researches.researches.length - 1)
            .id
        ..count = state.count
        ..showProgress = false
        ..loadMoreRequest = true));
      return true;
    }
  }

  Future<bool> _onWillPop() async {
    print('---------------------------------------------');
    print('research _onWillPop');
    print('---------------------------------------------');
    DateTime now = DateTime.now();
    if ((currentBackPressTime == null ||
        now.difference(currentBackPressTime) > Duration(seconds: 2))) {
      currentBackPressTime = now;
      return Future.value(false);
    } else {
      if (!_isReadyPdf)
        // SystemNavigator.pop();
        SystemChannels.platform.invokeMethod('SystemNavigator.pop');
      return Future.value(true);
    }
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    return WillPopScope(
      onWillPop: () {
        if (_isReadyPdf) {
          setState(() {
            _isReadyPdf = false;
          });
        }
        _onWillPop();
      },
      child: BlocBuilder(
          bloc: _bloc,
          builder: (BuildContext context, ResearchState state) {
            this.state = state;
            this.loadMoreRequest = state.loadMoreRequest;
            error(state.error);
            if (state.isLoading || state.isLoadingPdf) {
              return Center(
                child: SpinKitThreeBounce(
                  color: AmberColor,
                  size: ScreenUtil().setHeight(50),
                ),
              );
            }
            return Scaffold(
              backgroundColor: BackGroundColor,
              body: _isReadyPdf
                  ? PDFViewer(
                      document: _pdfDocument,
                      indicatorBackground: Colors.amber,
                      indicatorText: Colors.black,
                      zoomSteps: 1,
                    )
                  : ListView.builder(
                      controller: _scrollController,
                      padding: EdgeInsets.only(
                        top: ScreenUtil().setHeight(40),
                      ),
                      shrinkWrap: true,
                      itemCount: (state.researches != null &&
                              state.researches.researches != null)
                          ? state.researches.researches.length + 1
                          : 0,
                      itemBuilder: (BuildContext context, int index) => index ==
                              state.researches.researches.length
                          ? !state.isEmptyList
                              ? Center(
                                  child: Container(
                                  child: SpinKitThreeBounce(
                                    color: AmberColor,
                                    size: ScreenUtil().setHeight(50),
                                  ),
                                ))
                              : Container()
                          : Column(
                              children: [
                                Theme(
                                  data: Theme.of(context).copyWith(
                                      dividerColor: Colors.transparent),
                                  child: ExpansionTile(
                                    initiallyExpanded: state.researches
                                        .researches[index].isExpanded,
                                    onExpansionChanged: (expanded) {
                                      print(
                                          'ExpansionTile pressed!! expanded is:' +
                                              expanded.toString());
                                      _bloc.add(GetResearchIcon((b) => b
                                        ..isExpandedIcon = expanded
                                        ..indexIcon = state
                                            .researches.researches[index].id));
                                    },
                                    tilePadding: EdgeInsets.symmetric(
                                        horizontal:
                                            ScreenUtil().setHeight(20.0)),
                                    title: Text(
                                      state.researches.researches[index].title,
                                      textAlign: TextAlign.right,
                                      style: TextStyle(
                                        fontFamily: 'Al-Jazeera-Arabic',
                                        fontSize: ScreenUtil().setSp(45),
                                        color: BlackColor,
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                    trailing: IgnorePointer(
                                        child: Image.asset(
                                      state.researches.researches[index]
                                              .isExpanded
                                          ? PressedResearchButton
                                          : UnPressedResearchButton,
                                      fit: BoxFit.contain,
                                      height: ScreenUtil().setHeight(115),
                                      width: ScreenUtil().setWidth(115),
                                    )),
                                    children: <Widget>[
                                      cardWidget(
                                          state.researches.researches[index])
                                    ],
                                  ),
                                ),
                                state.researches.researches.length - 1 != index
                                    ? Divider(
                                        color: BlackColor,
                                        indent: ScreenUtil().setWidth(50.0),
                                        endIndent: ScreenUtil().setWidth(50.0),
                                      )
                                    : Container()
                              ],
                            ),
                    ),
            );
          }),
    );
  }

  Widget cardWidget(ResearchList research) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ButtonTheme(
              padding: EdgeInsets.only(
                top: ScreenUtil().setHeight(10),
                bottom: ScreenUtil().setHeight(10),
                right: ScreenUtil().setWidth(60),
                left: ScreenUtil().setWidth(60),
              ),
              child: RaisedButton(
                  textColor: BlackColor,
                  color: GrayColor,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30.0))),
                  onPressed: () {
                    setState(() {
                      _loadPdf(PdfUrl + research.file);
                    });
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'استعراض',
                        style: TextStyle(
                          fontFamily: 'Al-Jazeera-Arabic',
                          fontSize: ScreenUtil().setSp(40),
                          //  color: const Color(0xff363638),
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      SizedBox(
                        width: ScreenUtil().setWidth(40.0),
                      ),
                      Image.asset(
                        ViewButton,
                        fit: BoxFit.contain,
                        height: ScreenUtil().setHeight(40),
                        width: ScreenUtil().setWidth(40),
                      ),
                    ],
                  )),
            ),
            SizedBox(
              width: ScreenUtil().setWidth(30.0),
            ),
            ButtonTheme(
              padding: EdgeInsets.only(
                top: ScreenUtil().setHeight(10),
                bottom: ScreenUtil().setHeight(10),
                right: ScreenUtil().setWidth(90),
                left: ScreenUtil().setWidth(90),
              ),
              child: RaisedButton(
                  textColor: BlackColor,
                  color: AmberColor,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30.0))),
                  onPressed: () {
                    setState(() {
                      _bloc.add(
                          GetPdf((b) => b..pdfUrl = PdfUrl + research.file));
                    });
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'تحميل',
                        style: TextStyle(
                          fontFamily: 'Al-Jazeera-Arabic',
                          fontSize: ScreenUtil().setSp(40),
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      SizedBox(
                        width: ScreenUtil().setWidth(20.0),
                      ),
                      Image.asset(
                        DownloadButton,
                        fit: BoxFit.contain,
                        height: ScreenUtil().setHeight(40),
                        width: ScreenUtil().setWidth(40),
                      ),
                    ],
                  )),
            ),
          ],
        ),
        SizedBox(
          height: ScreenUtil().setHeight(10.0),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setHeight(60)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Flexible(
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Flexible(
                        flex: 5,
                        child: Text(
                          research.date + '  ',
                          style: TextStyle(
                            fontFamily: 'Al-Jazeera-Arabic',
                            fontSize: ScreenUtil().setSp(textSize),
                            color: Colors.black,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 10,
                        child: Text(
                          'تاريخ النشر' + ' ',
                          style: TextStyle(
                            fontFamily: 'Al-Jazeera-Arabic',
                            fontSize: ScreenUtil().setSp(textSize),
                            color: AmberColor,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child: Text(
                          '|',
                          style: TextStyle(
                            fontFamily: 'Al-Jazeera-Arabic',
                            fontSize: ScreenUtil().setSp(textSize),
                            color: const Color(0xff363638),
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                    ]),
              ),
              //////////////////////////////////////////////////
              Flexible(
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Flexible(
                        flex: 5,
                        child: Text(
                          research.company + '   ',
                          style: TextStyle(
                            fontFamily: 'Al-Jazeera-Arabic',
                            fontSize: ScreenUtil().setSp(textSize),
                            color: Colors.black,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 10,
                        child: Text(
                          'دار النشر' + '  ',
                          style: TextStyle(
                            fontFamily: 'Al-Jazeera-Arabic',
                            fontSize: ScreenUtil().setSp(textSize),
                            color: AmberColor,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child: Text(
                          '|',
                          style: TextStyle(
                            fontFamily: 'Al-Jazeera-Arabic',
                            fontSize: ScreenUtil().setSp(textSize),
                            color: const Color(0xff363638),
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                    ]),
              ),
              //////////////////////////////////////////////////
              Flexible(
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Flexible(
                        flex: 5,
                        child: Text(
                          research.author + '   ',
                          style: TextStyle(
                            fontFamily: 'Al-Jazeera-Arabic',
                            fontSize: ScreenUtil().setSp(textSize),
                            color: Colors.black,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 10,
                        child: Text(
                          ' ' + 'الكاتب',
                          style: TextStyle(
                            fontFamily: 'Al-Jazeera-Arabic',
                            fontSize: ScreenUtil().setSp(textSize),
                            color: AmberColor,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                    ]),
              ),
            ],
          ),
        ),
      ],
    );
  }

  void error(String errorCode) {
    if (errorCode.isNotEmpty) {
      Fluttertoast.showToast(
          msg: ErrorMessage,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: AmberColor,
          textColor: Colors.white,
          fontSize: 16.0);
      _bloc.add(ClearError());
    }
  }
}
