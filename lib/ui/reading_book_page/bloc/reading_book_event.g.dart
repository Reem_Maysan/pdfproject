// GENERATED CODE - DO NOT MODIFY BY HAND

part of reading_book_event;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ClearError extends ClearError {
  factory _$ClearError([void Function(ClearErrorBuilder) updates]) =>
      (new ClearErrorBuilder()..update(updates)).build();

  _$ClearError._() : super._();

  @override
  ClearError rebuild(void Function(ClearErrorBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ClearErrorBuilder toBuilder() => new ClearErrorBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ClearError;
  }

  @override
  int get hashCode {
    return 507656265;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('ClearError').toString();
  }
}

class ClearErrorBuilder implements Builder<ClearError, ClearErrorBuilder> {
  _$ClearError _$v;

  ClearErrorBuilder();

  @override
  void replace(ClearError other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ClearError;
  }

  @override
  void update(void Function(ClearErrorBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ClearError build() {
    final _$result = _$v ?? new _$ClearError._();
    replace(_$result);
    return _$result;
  }
}

class _$GetBooks extends GetBooks {
  @override
  final int count;
  @override
  final int start_id;
  @override
  final bool showProgress;
  @override
  final bool loadMoreRequest;

  factory _$GetBooks([void Function(GetBooksBuilder) updates]) =>
      (new GetBooksBuilder()..update(updates)).build();

  _$GetBooks._(
      {this.count, this.start_id, this.showProgress, this.loadMoreRequest})
      : super._() {
    if (count == null) {
      throw new BuiltValueNullFieldError('GetBooks', 'count');
    }
    if (start_id == null) {
      throw new BuiltValueNullFieldError('GetBooks', 'start_id');
    }
    if (showProgress == null) {
      throw new BuiltValueNullFieldError('GetBooks', 'showProgress');
    }
    if (loadMoreRequest == null) {
      throw new BuiltValueNullFieldError('GetBooks', 'loadMoreRequest');
    }
  }

  @override
  GetBooks rebuild(void Function(GetBooksBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetBooksBuilder toBuilder() => new GetBooksBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetBooks &&
        count == other.count &&
        start_id == other.start_id &&
        showProgress == other.showProgress &&
        loadMoreRequest == other.loadMoreRequest;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, count.hashCode), start_id.hashCode),
            showProgress.hashCode),
        loadMoreRequest.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GetBooks')
          ..add('count', count)
          ..add('start_id', start_id)
          ..add('showProgress', showProgress)
          ..add('loadMoreRequest', loadMoreRequest))
        .toString();
  }
}

class GetBooksBuilder implements Builder<GetBooks, GetBooksBuilder> {
  _$GetBooks _$v;

  int _count;
  int get count => _$this._count;
  set count(int count) => _$this._count = count;

  int _start_id;
  int get start_id => _$this._start_id;
  set start_id(int start_id) => _$this._start_id = start_id;

  bool _showProgress;
  bool get showProgress => _$this._showProgress;
  set showProgress(bool showProgress) => _$this._showProgress = showProgress;

  bool _loadMoreRequest;
  bool get loadMoreRequest => _$this._loadMoreRequest;
  set loadMoreRequest(bool loadMoreRequest) =>
      _$this._loadMoreRequest = loadMoreRequest;

  GetBooksBuilder();

  GetBooksBuilder get _$this {
    if (_$v != null) {
      _count = _$v.count;
      _start_id = _$v.start_id;
      _showProgress = _$v.showProgress;
      _loadMoreRequest = _$v.loadMoreRequest;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetBooks other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetBooks;
  }

  @override
  void update(void Function(GetBooksBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetBooks build() {
    final _$result = _$v ??
        new _$GetBooks._(
            count: count,
            start_id: start_id,
            showProgress: showProgress,
            loadMoreRequest: loadMoreRequest);
    replace(_$result);
    return _$result;
  }
}

class _$GetBookIcon extends GetBookIcon {
  @override
  final bool isExpandedIcon;
  @override
  final int indexIcon;

  factory _$GetBookIcon([void Function(GetBookIconBuilder) updates]) =>
      (new GetBookIconBuilder()..update(updates)).build();

  _$GetBookIcon._({this.isExpandedIcon, this.indexIcon}) : super._() {
    if (isExpandedIcon == null) {
      throw new BuiltValueNullFieldError('GetBookIcon', 'isExpandedIcon');
    }
    if (indexIcon == null) {
      throw new BuiltValueNullFieldError('GetBookIcon', 'indexIcon');
    }
  }

  @override
  GetBookIcon rebuild(void Function(GetBookIconBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetBookIconBuilder toBuilder() => new GetBookIconBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetBookIcon &&
        isExpandedIcon == other.isExpandedIcon &&
        indexIcon == other.indexIcon;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, isExpandedIcon.hashCode), indexIcon.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GetBookIcon')
          ..add('isExpandedIcon', isExpandedIcon)
          ..add('indexIcon', indexIcon))
        .toString();
  }
}

class GetBookIconBuilder implements Builder<GetBookIcon, GetBookIconBuilder> {
  _$GetBookIcon _$v;

  bool _isExpandedIcon;
  bool get isExpandedIcon => _$this._isExpandedIcon;
  set isExpandedIcon(bool isExpandedIcon) =>
      _$this._isExpandedIcon = isExpandedIcon;

  int _indexIcon;
  int get indexIcon => _$this._indexIcon;
  set indexIcon(int indexIcon) => _$this._indexIcon = indexIcon;

  GetBookIconBuilder();

  GetBookIconBuilder get _$this {
    if (_$v != null) {
      _isExpandedIcon = _$v.isExpandedIcon;
      _indexIcon = _$v.indexIcon;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetBookIcon other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetBookIcon;
  }

  @override
  void update(void Function(GetBookIconBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetBookIcon build() {
    final _$result = _$v ??
        new _$GetBookIcon._(
            isExpandedIcon: isExpandedIcon, indexIcon: indexIcon);
    replace(_$result);
    return _$result;
  }
}

class _$GetPdf extends GetPdf {
  @override
  final String pdfPath;

  factory _$GetPdf([void Function(GetPdfBuilder) updates]) =>
      (new GetPdfBuilder()..update(updates)).build();

  _$GetPdf._({this.pdfPath}) : super._() {
    if (pdfPath == null) {
      throw new BuiltValueNullFieldError('GetPdf', 'pdfPath');
    }
  }

  @override
  GetPdf rebuild(void Function(GetPdfBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetPdfBuilder toBuilder() => new GetPdfBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetPdf && pdfPath == other.pdfPath;
  }

  @override
  int get hashCode {
    return $jf($jc(0, pdfPath.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GetPdf')..add('pdfPath', pdfPath))
        .toString();
  }
}

class GetPdfBuilder implements Builder<GetPdf, GetPdfBuilder> {
  _$GetPdf _$v;

  String _pdfPath;
  String get pdfPath => _$this._pdfPath;
  set pdfPath(String pdfPath) => _$this._pdfPath = pdfPath;

  GetPdfBuilder();

  GetPdfBuilder get _$this {
    if (_$v != null) {
      _pdfPath = _$v.pdfPath;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetPdf other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetPdf;
  }

  @override
  void update(void Function(GetPdfBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetPdf build() {
    final _$result = _$v ?? new _$GetPdf._(pdfPath: pdfPath);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
