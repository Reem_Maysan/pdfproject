library reading_book_event;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'reading_book_event.g.dart';

abstract class ReadingBookEvent {}

abstract class ClearError extends ReadingBookEvent
    implements Built<ClearError, ClearErrorBuilder> {
  // fields go here

  ClearError._();

  factory ClearError([updates(ClearErrorBuilder b)]) = _$ClearError;
}


abstract class GetBooks extends ReadingBookEvent
    implements Built<GetBooks, GetBooksBuilder> {
  // fields go here
  
  int get count;

  int get start_id;

  bool get showProgress;

  bool get loadMoreRequest;

  GetBooks._();

  factory GetBooks([updates(GetBooksBuilder b)]) = _$GetBooks;
}


abstract class GetBookIcon extends ReadingBookEvent
    implements Built<GetBookIcon, GetBookIconBuilder> {
  // fields go here
  
  bool get isExpandedIcon;
  int get indexIcon;

  GetBookIcon._();

  factory GetBookIcon([updates(GetBookIconBuilder b)]) = _$GetBookIcon;
}

abstract class GetPdf extends ReadingBookEvent
    implements Built<GetPdf, GetPdfBuilder> {
  // fields go here

  String get pdfPath;

  GetPdf._();

  factory GetPdf([updates(GetPdfBuilder b)]) = _$GetPdf;
}
