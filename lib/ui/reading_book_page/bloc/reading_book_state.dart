library reading_book_state;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:pdfproject/models/book_model/book_model.dart';
import 'package:pdfproject/models/serializer/serializer.dart';

part 'reading_book_state.g.dart';

abstract class ReadingBookState
    implements Built<ReadingBookState, ReadingBookStateBuilder> {
  // fields go here

  String get error;

  bool get isLoading;

  bool get isLoadingPdf;

  @nullable
  BookModel get book;

  int get count;

  int get start_id;

  bool get isEmptyList;

  bool get isExpanded;

  bool get loadMoreRequest;
  ReadingBookState._();

  factory ReadingBookState([updates(ReadingBookStateBuilder b)]) =
      _$ReadingBookState;

  factory ReadingBookState.initail() {
    return ReadingBookState((b) => b
      ..error = ""
      ..isLoading = false
      ..book = null
      ..count = 2
      ..start_id = 0
      ..isEmptyList = false
      ..isExpanded= false
      ..loadMoreRequest = false
    ..isLoadingPdf=false);
  }
}
