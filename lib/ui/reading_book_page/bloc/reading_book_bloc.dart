import 'dart:io';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pdfproject/data/repository/irepository.dart';
import 'package:pdfproject/models/book_model/book_list.dart';
import 'package:pdfproject/ui/reading_book_page/bloc/reading_book_event.dart';
import 'package:pdfproject/ui/reading_book_page/bloc/reading_book_state.dart';

class ReadingBookBloc extends Bloc<ReadingBookEvent, ReadingBookState> {
  IRepository _repository;

  ReadingBookBloc(this._repository);

  @override
  ReadingBookState get initialState => ReadingBookState.initail();

  @override
  Stream<ReadingBookState> mapEventToState(ReadingBookEvent event) async* {
    if (event is ClearError) {
      yield state.rebuild((b) => b..error = "");
    }
    if (event is GetBooks && !state.isEmptyList) {
      try {
        yield state.rebuild((b) => b
          ..isLoading = event.showProgress
          ..error = "");
        final data = await _repository.getBook(event.start_id, event.count);

        yield data.books.isEmpty
            ? state.rebuild((b) => b..isEmptyList = true)
            : state.rebuild((b) => b
              ..isLoading = false
              ..error = ""
              ..count = event.count
              ..start_id = event.start_id
              ..loadMoreRequest = false
              ..book.update((b) {
                if (!state.isEmptyList) {
                  b..books.addAll(data.books);
                  b..books.map((f) => f.rebuild((b) => b..isExpanded = false));
                }
              })
              ..isEmptyList = data.books.isEmpty);
      } catch (e) {
        print('GetBooks Error $e');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = "Something went wrong");
      }
    }
    if (event is GetBookIcon) {
      try {
        yield state.rebuild((b) => b
          ..book.update((b) {
            if (!state.isEmptyList) {
              b
                ..books.map((f) {
                  if (f.id == event.indexIcon) {
                    print('weeee fouuund it!! its: ' +
                        f.id.toString() +
                        'b..isExpanded : ' +
                        f.isExpanded.toString() +
                        ' become :' +
                        event.isExpandedIcon.toString());
                    return f
                        .rebuild((b) => b..isExpanded = event.isExpandedIcon);
                  } else
                    return f.rebuild((b) => b..isExpanded = b.isExpanded);
                });
            }
          }));
      } catch (e) {
        print('GetBookIcon Error $e');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = "Something went wrong");
      }
    }
    if (event is GetPdf) {
      try {
        yield state.rebuild((b) => b..isLoadingPdf = true);
        final data = await _repository.getPdf(event.pdfPath);
        yield state.rebuild((b) => b..isLoadingPdf = false);
        print('GetPdf response $data');
      } catch (e) {
        print('GetPdf Error $e');
        yield state.rebuild((b) => b..error = "Something went wrong");
      }
    }
  }
}
