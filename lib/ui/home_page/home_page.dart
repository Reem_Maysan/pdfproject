import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pdfproject/constents/constent.dart';
import 'package:pdfproject/ui/Enterprise_Publications_pages/publications.dart';
import 'package:pdfproject/ui/articles_page/articles_page.dart';
import 'package:pdfproject/ui/reading_book_page/reading_book_page.dart';
import 'package:pdfproject/ui/research_page/research_page.dart';
import 'package:pdfproject/ui/welcome_page/welcome_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool once;
  double _preferredHieght = 100.0;
  int _currentIndx = 0;
  DateTime currentBackPressTime;

  String tabImage1 = 'assets/Icons/s-art.png';
  String tabImage2 = 'assets/Icons/uns-write.png';
  String tabImage3 = 'assets/Icons/uns-read.png';
  String tabImage4 = 'assets/Icons/uns-book.png';
  String tabImage5 = 'assets/Icons/uns-globe.png';

  final List tabs = [
    ArticlesPage(),
    ReadingBookPage(),
    PublicationsPage(),
    ResearchesAndStudiesPage(),
    WelcomePage(
      launchUrl: true,
    ),
  ];

  final List tabsTitles = [
    'المقالات',
    'قراءة في الكتاب',
    'اصدارات المؤسسة',
    'أبحاث',
    'صفحةالويب'
  ];

  final List appBarTabsImages = [
    'assets/Icons/uns-art.png',
    'assets/Icons/s-g-write.png',
    'assets/Icons/s-g-read.png',
    'assets/Icons/s-g-book.png',
    'assets/Icons/uns-globe.png'
  ];

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    return SafeArea(
      child: Scaffold(
          backgroundColor: BackGroundColor,
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(_preferredHieght),
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ListTile(
                    tileColor: BlackColor,
                    trailing: Image.asset('assets/Images/logo5.png',
                        fit: BoxFit.fitWidth,
                        height: ScreenUtil().setHeight(130),
                        width: ScreenUtil().setWidth(130)),
                  ),
                  Flexible(
                    child: Container(
                        padding:
                            EdgeInsets.only(right: ScreenUtil().setWidth(10)),
                        color: BackGroundColor,
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                this.tabsTitles[_currentIndx],
                                style: TextStyle(
                                  fontFamily: 'Al-Jazeera-Arabic',
                                  fontSize: ScreenUtil().setSp(68),
                                  color: Colors.black,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              SizedBox(
                                width: ScreenUtil().setWidth(50),
                              ),
                              _currentIndx != 0
                                  ? Image.asset(
                                      this.appBarTabsImages[_currentIndx],
                                      fit: BoxFit.cover,
                                      height: ScreenUtil().setHeight(80),
                                      width: ScreenUtil().setWidth(80),
                                    )
                                  : Container()
                            ])),
                  ),
                ],
              ),
            ),
          ),
          bottomNavigationBar: BottomNavigationBar(
            currentIndex: _currentIndx,
            items:
            [
              BottomNavigationBarItem(
                icon: Image.asset(
                  tabImage1,
                  fit: BoxFit.contain,
                  height: ScreenUtil().setHeight(80),
                  width: ScreenUtil().setWidth(80),
                ),
                title: Text(""),
              ),
              BottomNavigationBarItem(
                icon: Image.asset(
                  tabImage2,
                  fit: BoxFit.contain,
                  height: ScreenUtil().setHeight(80),
                  width: ScreenUtil().setWidth(80),
                ),
                title: Text(""),
              ),
              BottomNavigationBarItem(
                icon: Image.asset(
                  tabImage3,
                  fit: BoxFit.contain,
                  height: ScreenUtil().setHeight(80),
                  width: ScreenUtil().setWidth(80),
                ),
                title: Text(""),
              ),
              BottomNavigationBarItem(
                icon: Image.asset(
                  tabImage4,
                  fit: BoxFit.contain,
                  height: ScreenUtil().setHeight(80),
                  width: ScreenUtil().setWidth(80),
                ),
                title: Text(""),
              ),
              BottomNavigationBarItem(
                icon: Image.asset(
                  tabImage5,
                  fit: BoxFit.contain,
                  height: ScreenUtil().setHeight(80),
                  width: ScreenUtil().setWidth(80),
                ),
                title: Text(""),
              ),
            ],
            type: BottomNavigationBarType.fixed,
            onTap: (index) {
              setState(() {
                _currentIndx = index;
                if (index == 0) {
                  tabImage1 = 'assets/Icons/s-art.png';
                  tabImage2 = 'assets/Icons/uns-write.png';
                  tabImage3 = 'assets/Icons/uns-read.png';
                  tabImage4 = 'assets/Icons/uns-book.png';
                  tabImage5 = 'assets/Icons/uns-globe.png';
                } else if (index == 1) {
                  tabImage1 = 'assets/Icons/uns-art.png';
                  tabImage2 = 'assets/Icons/s-o-write.png';
                  tabImage3 = 'assets/Icons/uns-read.png';
                  tabImage4 = 'assets/Icons/uns-book.png';
                  tabImage5 = 'assets/Icons/uns-globe.png';
                } else if (index == 2) {
                  tabImage1 = 'assets/Icons/uns-art.png';
                  tabImage2 = 'assets/Icons/uns-write.png';
                  tabImage3 = 'assets/Icons/s-g-read.png';
                  tabImage4 = 'assets/Icons/uns-book.png';
                  tabImage5 = 'assets/Icons/uns-globe.png';
                } else if (index == 3) {
                  tabImage1 = 'assets/Icons/uns-art.png';
                  tabImage2 = 'assets/Icons/uns-write.png';
                  tabImage4 = 'assets/Icons/s-o-book.png';
                  tabImage3 = 'assets/Icons/uns-read.png';
                  tabImage5 = 'assets/Icons/uns-globe.png';
                } else if (index == 4) {
                  tabImage1 = 'assets/Icons/uns-art.png';
                  tabImage2 = 'assets/Icons/uns-write.png';
                  tabImage3 = 'assets/Icons/uns-read.png';
                  tabImage4 = 'assets/Icons/uns-book.png';
                  tabImage5 = 'assets/Icons/uns-globe.png';
                }
              });
            },
          ),
          body: tabs.elementAt(_currentIndx)),
    );
  }

  Future<bool> _onWillPop() async {
    print('---------------------------------------------');
    print('home _onWillPop');
    print('---------------------------------------------');

    DateTime now = DateTime.now();
      if ((currentBackPressTime == null ||
          now.difference(currentBackPressTime) > Duration(seconds: 2))) {
        currentBackPressTime = now;
        Fluttertoast.showToast(
            msg: 'اضغط مجددا للخروج',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIos: 1,
            backgroundColor: AmberColor,
            textColor: Colors.white,
            fontSize: 16.0);
        return Future.value(false);
      }
    return Future.value(true);
  }
}
