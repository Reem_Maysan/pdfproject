import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pdfproject/constents/constent.dart';
import 'package:pdfproject/models/post_model/post_list.dart';

class ArticleContent extends StatefulWidget {
  final PostList postItem;

  const ArticleContent({Key key, @required this.postItem}) : super(key: key);

  @override
  _ArticleContentState createState() => _ArticleContentState();
}

class _ArticleContentState extends State<ArticleContent> {

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: ScreenUtil().setHeight(800),
                width: MediaQuery.of(context).size.width,
                child: Image.network(
                  ImageUrl + widget.postItem.image,
                  fit: BoxFit.cover,
                ),
              ),
              Container(
                color: Colors.white,
                child: Container(
                  margin: EdgeInsets.only(
                      top: ScreenUtil().setHeight(10.0),
                      bottom: ScreenUtil().setHeight(10.0),
                      right: ScreenUtil().setWidth(30.0),
                      left: ScreenUtil().setWidth(30.0)),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          widget.postItem.title,
                          style: TextStyle(
                            fontFamily: 'Al-Jazeera-Arabic',
                            fontSize: ScreenUtil().setSp(60),
                            color: Colors.black,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        SizedBox(height: ScreenUtil().setHeight(10.0)),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Padding(
                              padding: EdgeInsetsDirectional.only(
                                  start: ScreenUtil().setWidth(50)),
                              child: Text(
                                  widget.postItem.created_at.substring(0, 10),
                                style: TextStyle(
                                  fontFamily: 'Al-Jazeera-Arabic',
                                  fontSize: ScreenUtil().setSp(40),
                                  color: Colors.black,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  widget.postItem.user.name,
                                  style: TextStyle(
                                    fontFamily: 'Al-Jazeera-Arabic',
                                    fontSize: ScreenUtil().setSp(40),
                                    color: Colors.black,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                SizedBox(width: ScreenUtil().setWidth(10.0)),
                                CircleAvatar(
                                    radius: 20,
                                    backgroundImage: NetworkImage(ImageUrl + widget.postItem.user.image,)
                                )
                              /*  ClipRRect(
                                  borderRadius: BorderRadius.circular(4.0),
                                  child: Image.network(
                                    ImageUrl + widget.postItem.user.image,
                                   // PressedPublicationButton,
                                    height: ScreenUtil().setHeight(90),
                                    width: ScreenUtil().setWidth(90),
                                  ),
                                )*/
                              ],
                            ),
                          ],
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(40.0),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width*0.8,
                         // padding: const EdgeInsets.all(16.0),
                          padding: EdgeInsets.only(
                              top: ScreenUtil().setHeight(16.0),
                              bottom: ScreenUtil().setHeight(16.0),
                              right: ScreenUtil().setWidth(16.0),
                              left: ScreenUtil().setWidth(16.0)),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text(
                                widget.postItem.description,
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                  fontFamily: 'Al-Jazeera-Arabic',
                                  fontSize: ScreenUtil().setSp(textSize),
                                  color: Colors.black,
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                ),
              ),
            ],
          ),
        ),),
    );
  }
}
