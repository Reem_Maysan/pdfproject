library articles_event;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:pdfproject/models/serializer/serializer.dart';

part 'articles_event.g.dart';

abstract class ArticlesEvent {}

abstract class ClearError extends ArticlesEvent
    implements Built<ClearError, ClearErrorBuilder> {
  // fields go here

  ClearError._();

  factory ClearError([updates(ClearErrorBuilder b)]) = _$ClearError;
}

abstract class GetPosts extends ArticlesEvent
    implements Built<GetPosts, GetPostsBuilder> {
  // fields go here

  int get count; 
  
  int get start_id;

  bool get showProgress;

  bool get loadMoreRequest;

  GetPosts._();

  factory GetPosts([updates(GetPostsBuilder b)]) = _$GetPosts;
}

abstract class GetPostById extends ArticlesEvent
    implements Built<GetPostById, GetPostByIdBuilder> {
  // fields go here

  int get articleId;

  GetPostById._();

  factory GetPostById([updates(GetPostByIdBuilder b)]) = _$GetPostById;
}

