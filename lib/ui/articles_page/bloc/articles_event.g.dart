// GENERATED CODE - DO NOT MODIFY BY HAND

part of articles_event;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ClearError extends ClearError {
  factory _$ClearError([void Function(ClearErrorBuilder) updates]) =>
      (new ClearErrorBuilder()..update(updates)).build();

  _$ClearError._() : super._();

  @override
  ClearError rebuild(void Function(ClearErrorBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ClearErrorBuilder toBuilder() => new ClearErrorBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ClearError;
  }

  @override
  int get hashCode {
    return 507656265;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('ClearError').toString();
  }
}

class ClearErrorBuilder implements Builder<ClearError, ClearErrorBuilder> {
  _$ClearError _$v;

  ClearErrorBuilder();

  @override
  void replace(ClearError other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ClearError;
  }

  @override
  void update(void Function(ClearErrorBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ClearError build() {
    final _$result = _$v ?? new _$ClearError._();
    replace(_$result);
    return _$result;
  }
}

class _$GetPosts extends GetPosts {
  @override
  final int count;
  @override
  final int start_id;
  @override
  final bool showProgress;
  @override
  final bool loadMoreRequest;

  factory _$GetPosts([void Function(GetPostsBuilder) updates]) =>
      (new GetPostsBuilder()..update(updates)).build();

  _$GetPosts._(
      {this.count, this.start_id, this.showProgress, this.loadMoreRequest})
      : super._() {
    if (count == null) {
      throw new BuiltValueNullFieldError('GetPosts', 'count');
    }
    if (start_id == null) {
      throw new BuiltValueNullFieldError('GetPosts', 'start_id');
    }
    if (showProgress == null) {
      throw new BuiltValueNullFieldError('GetPosts', 'showProgress');
    }
    if (loadMoreRequest == null) {
      throw new BuiltValueNullFieldError('GetPosts', 'loadMoreRequest');
    }
  }

  @override
  GetPosts rebuild(void Function(GetPostsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetPostsBuilder toBuilder() => new GetPostsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetPosts &&
        count == other.count &&
        start_id == other.start_id &&
        showProgress == other.showProgress &&
        loadMoreRequest == other.loadMoreRequest;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, count.hashCode), start_id.hashCode),
            showProgress.hashCode),
        loadMoreRequest.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GetPosts')
          ..add('count', count)
          ..add('start_id', start_id)
          ..add('showProgress', showProgress)
          ..add('loadMoreRequest', loadMoreRequest))
        .toString();
  }
}

class GetPostsBuilder implements Builder<GetPosts, GetPostsBuilder> {
  _$GetPosts _$v;

  int _count;
  int get count => _$this._count;
  set count(int count) => _$this._count = count;

  int _start_id;
  int get start_id => _$this._start_id;
  set start_id(int start_id) => _$this._start_id = start_id;

  bool _showProgress;
  bool get showProgress => _$this._showProgress;
  set showProgress(bool showProgress) => _$this._showProgress = showProgress;

  bool _loadMoreRequest;
  bool get loadMoreRequest => _$this._loadMoreRequest;
  set loadMoreRequest(bool loadMoreRequest) =>
      _$this._loadMoreRequest = loadMoreRequest;

  GetPostsBuilder();

  GetPostsBuilder get _$this {
    if (_$v != null) {
      _count = _$v.count;
      _start_id = _$v.start_id;
      _showProgress = _$v.showProgress;
      _loadMoreRequest = _$v.loadMoreRequest;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetPosts other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetPosts;
  }

  @override
  void update(void Function(GetPostsBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetPosts build() {
    final _$result = _$v ??
        new _$GetPosts._(
            count: count,
            start_id: start_id,
            showProgress: showProgress,
            loadMoreRequest: loadMoreRequest);
    replace(_$result);
    return _$result;
  }
}

class _$GetPostById extends GetPostById {
  @override
  final int articleId;

  factory _$GetPostById([void Function(GetPostByIdBuilder) updates]) =>
      (new GetPostByIdBuilder()..update(updates)).build();

  _$GetPostById._({this.articleId}) : super._() {
    if (articleId == null) {
      throw new BuiltValueNullFieldError('GetPostById', 'articleId');
    }
  }

  @override
  GetPostById rebuild(void Function(GetPostByIdBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetPostByIdBuilder toBuilder() => new GetPostByIdBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetPostById && articleId == other.articleId;
  }

  @override
  int get hashCode {
    return $jf($jc(0, articleId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GetPostById')
          ..add('articleId', articleId))
        .toString();
  }
}

class GetPostByIdBuilder implements Builder<GetPostById, GetPostByIdBuilder> {
  _$GetPostById _$v;

  int _articleId;
  int get articleId => _$this._articleId;
  set articleId(int articleId) => _$this._articleId = articleId;

  GetPostByIdBuilder();

  GetPostByIdBuilder get _$this {
    if (_$v != null) {
      _articleId = _$v.articleId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetPostById other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetPostById;
  }

  @override
  void update(void Function(GetPostByIdBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetPostById build() {
    final _$result = _$v ?? new _$GetPostById._(articleId: articleId);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
