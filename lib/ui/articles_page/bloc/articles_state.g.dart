// GENERATED CODE - DO NOT MODIFY BY HAND

part of articles_state;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ArticlesState extends ArticlesState {
  @override
  final String error;
  @override
  final bool isLoading;
  @override
  final PostModel post;
  @override
  final PostList postItemList;
  @override
  final int articleId;
  @override
  final int count;
  @override
  final bool isEmptyList;
  @override
  final int start_id;
  @override
  final bool loadMoreRequest;

  factory _$ArticlesState([void Function(ArticlesStateBuilder) updates]) =>
      (new ArticlesStateBuilder()..update(updates)).build();

  _$ArticlesState._(
      {this.error,
      this.isLoading,
      this.post,
      this.postItemList,
      this.articleId,
      this.count,
      this.isEmptyList,
      this.start_id,
      this.loadMoreRequest})
      : super._() {
    if (error == null) {
      throw new BuiltValueNullFieldError('ArticlesState', 'error');
    }
    if (isLoading == null) {
      throw new BuiltValueNullFieldError('ArticlesState', 'isLoading');
    }
    if (articleId == null) {
      throw new BuiltValueNullFieldError('ArticlesState', 'articleId');
    }
    if (count == null) {
      throw new BuiltValueNullFieldError('ArticlesState', 'count');
    }
    if (isEmptyList == null) {
      throw new BuiltValueNullFieldError('ArticlesState', 'isEmptyList');
    }
    if (start_id == null) {
      throw new BuiltValueNullFieldError('ArticlesState', 'start_id');
    }
    if (loadMoreRequest == null) {
      throw new BuiltValueNullFieldError('ArticlesState', 'loadMoreRequest');
    }
  }

  @override
  ArticlesState rebuild(void Function(ArticlesStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ArticlesStateBuilder toBuilder() => new ArticlesStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ArticlesState &&
        error == other.error &&
        isLoading == other.isLoading &&
        post == other.post &&
        postItemList == other.postItemList &&
        articleId == other.articleId &&
        count == other.count &&
        isEmptyList == other.isEmptyList &&
        start_id == other.start_id &&
        loadMoreRequest == other.loadMoreRequest;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc($jc($jc(0, error.hashCode), isLoading.hashCode),
                                post.hashCode),
                            postItemList.hashCode),
                        articleId.hashCode),
                    count.hashCode),
                isEmptyList.hashCode),
            start_id.hashCode),
        loadMoreRequest.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ArticlesState')
          ..add('error', error)
          ..add('isLoading', isLoading)
          ..add('post', post)
          ..add('postItemList', postItemList)
          ..add('articleId', articleId)
          ..add('count', count)
          ..add('isEmptyList', isEmptyList)
          ..add('start_id', start_id)
          ..add('loadMoreRequest', loadMoreRequest))
        .toString();
  }
}

class ArticlesStateBuilder
    implements Builder<ArticlesState, ArticlesStateBuilder> {
  _$ArticlesState _$v;

  String _error;
  String get error => _$this._error;
  set error(String error) => _$this._error = error;

  bool _isLoading;
  bool get isLoading => _$this._isLoading;
  set isLoading(bool isLoading) => _$this._isLoading = isLoading;

  PostModelBuilder _post;
  PostModelBuilder get post => _$this._post ??= new PostModelBuilder();
  set post(PostModelBuilder post) => _$this._post = post;

  PostListBuilder _postItemList;
  PostListBuilder get postItemList =>
      _$this._postItemList ??= new PostListBuilder();
  set postItemList(PostListBuilder postItemList) =>
      _$this._postItemList = postItemList;

  int _articleId;
  int get articleId => _$this._articleId;
  set articleId(int articleId) => _$this._articleId = articleId;

  int _count;
  int get count => _$this._count;
  set count(int count) => _$this._count = count;

  bool _isEmptyList;
  bool get isEmptyList => _$this._isEmptyList;
  set isEmptyList(bool isEmptyList) => _$this._isEmptyList = isEmptyList;

  int _start_id;
  int get start_id => _$this._start_id;
  set start_id(int start_id) => _$this._start_id = start_id;

  bool _loadMoreRequest;
  bool get loadMoreRequest => _$this._loadMoreRequest;
  set loadMoreRequest(bool loadMoreRequest) =>
      _$this._loadMoreRequest = loadMoreRequest;

  ArticlesStateBuilder();

  ArticlesStateBuilder get _$this {
    if (_$v != null) {
      _error = _$v.error;
      _isLoading = _$v.isLoading;
      _post = _$v.post?.toBuilder();
      _postItemList = _$v.postItemList?.toBuilder();
      _articleId = _$v.articleId;
      _count = _$v.count;
      _isEmptyList = _$v.isEmptyList;
      _start_id = _$v.start_id;
      _loadMoreRequest = _$v.loadMoreRequest;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ArticlesState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ArticlesState;
  }

  @override
  void update(void Function(ArticlesStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ArticlesState build() {
    _$ArticlesState _$result;
    try {
      _$result = _$v ??
          new _$ArticlesState._(
              error: error,
              isLoading: isLoading,
              post: _post?.build(),
              postItemList: _postItemList?.build(),
              articleId: articleId,
              count: count,
              isEmptyList: isEmptyList,
              start_id: start_id,
              loadMoreRequest: loadMoreRequest);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'post';
        _post?.build();
        _$failedField = 'postItemList';
        _postItemList?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ArticlesState', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
