import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pdfproject/constents/constent.dart';
import 'package:pdfproject/data/repository/irepository.dart';
import 'package:pdfproject/ui/articles_page/bloc/articles_event.dart';
import 'package:pdfproject/ui/articles_page/bloc/articles_state.dart';

class ArticlesBloc extends Bloc<ArticlesEvent, ArticlesState> {
  IRepository _repository;

  ArticlesBloc(this._repository);

  @override
  ArticlesState get initialState => ArticlesState.initail();

  @override
  Stream<ArticlesState> mapEventToState(ArticlesEvent event) async* {
    if (event is ClearError) {
      yield state.rebuild((b) => b..error = "");
    }
    if (event is GetPosts && !state.isEmptyList) {
      try {
        yield state.rebuild((b) => b
          ..isLoading = event.showProgress
          ..error = "");
        final data = await _repository.getPost(event.start_id, event.count);

        yield data.data.isEmpty
            ? state.rebuild((b) => b..isEmptyList = true)
            : state.rebuild((b) => b
              ..isLoading = false
              ..error = ""
              ..count = event.count
              ..start_id = event.start_id
              ..loadMoreRequest = false
              ..post.update((b) {
               if(!state.isEmptyList) b..data.addAll(data.data);
              })
        );
      } catch (e) {
        print('GetPosts Error $e');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = "Something went wrong");
      }
    }
    if (event is GetPostById) {
      try {
        yield state.rebuild((b) => b
          ..isLoading = true
          ..error = ""
          ..postItemList = null);
        final data = await _repository.getPostById(event.articleId);
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = ""
          ..articleId = event.articleId
          ..postItemList.replace(data));
      } catch (e) {
        print('GetPostById Error $e');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = "Something went wrong"
          ..postItemList = null);
      }
    }
  }
}
