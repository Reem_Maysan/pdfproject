library articles_state;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:pdfproject/models/post_model/post_list.dart';
import 'package:pdfproject/models/post_model/post_list.dart';
import 'package:pdfproject/models/post_model/post_model.dart';

part 'articles_state.g.dart';

abstract class ArticlesState
    implements Built<ArticlesState, ArticlesStateBuilder> {
  String get error;

  bool get isLoading;

  @nullable
  PostModel get post;

  @nullable
  PostList get postItemList;

  int get articleId;

  int get count;

  bool get isEmptyList;

  int get start_id;

  bool get loadMoreRequest;

  ArticlesState._();

  factory ArticlesState([updates(ArticlesStateBuilder b)]) = _$ArticlesState;

  factory ArticlesState.initail() {
    return ArticlesState((b) => b
      ..error = ""
      ..isLoading = false
      ..post = null
      ..articleId = 0
      ..count = 2
      ..start_id=0
      ..loadMoreRequest = false
      ..isEmptyList = false
      ..postItemList = null);
  }
}
