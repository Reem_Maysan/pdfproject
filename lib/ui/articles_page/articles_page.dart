import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:loadmore/loadmore.dart';
import 'package:pdfproject/constents/constent.dart';
import 'package:pdfproject/injectoin.dart';
import 'package:pdfproject/models/base/MyLoadMoreDelegate.dart';
import 'package:pdfproject/models/base/MyProgressDialogIndicator.dart';
import 'package:pdfproject/models/post_model/post_list.dart';
import 'package:pdfproject/ui/articles_page/article_content_page.dart';
import 'package:pdfproject/ui/articles_page/bloc/articles_bloc.dart';
import 'package:pdfproject/ui/articles_page/bloc/articles_event.dart';
import 'package:pdfproject/ui/articles_page/bloc/articles_state.dart';

class ArticlesPage extends StatefulWidget {
  @override
  _ArticlesPageState createState() => _ArticlesPageState();
}

class _ArticlesPageState extends State<ArticlesPage> {
  final _bloc = sl<ArticlesBloc>();
  ArticlesState state;
  bool showProgressLoadMore = true;
  final _scrollController = ScrollController();
  final _scrollThreshold = 200.0;
  bool loadMoreRequest = false;
  DateTime currentBackPressTime;

  @override
  void initState() {
    _scrollController.addListener(_onScroll);
    _bloc.add(GetPosts((b) {
      b
        ..start_id = 0
        ..count = 10
        ..loadMoreRequest = false
        ..showProgress = true;
    }));
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll <= _scrollThreshold && !loadMoreRequest) {
      _loadMore();
    }
  }

  Future<bool> _loadMore() async {
    loadMoreRequest = true;
    print('-loadMore');
    if (state.post != null && (state.isLoading || state.isEmptyList)) {
      return false;
    }
    if (state.post != null) {
      _bloc.add(GetPosts((b) => b
        ..start_id = state.post.data.elementAt(state.post.data.length - 1).id
        ..count = state.count
        ..showProgress = false
        ..loadMoreRequest = true));
      return true;
    }
  }

  Future<bool> _onWillPop() async {
    print('---------------------------------------------');
    print('article _onWillPop');
    print('---------------------------------------------');

    DateTime now = DateTime.now();
    if ((currentBackPressTime == null ||
        now.difference(currentBackPressTime) > Duration(seconds: 2))) {
      currentBackPressTime = now;
      return Future.value(false);
    }
    return Future.value(true);
  }


  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    return WillPopScope(
      onWillPop: _onWillPop ,
      child: BlocBuilder(
          bloc: _bloc,
          builder: (BuildContext context, ArticlesState state) {
            this.state = state;
            this.loadMoreRequest = state.loadMoreRequest;
            error(state.error);

            if (state.isLoading) {
              return Center(
                child: SpinKitThreeBounce(
                  color: AmberColor,
                  size: ScreenUtil().setHeight(50),
                ),
              );
            }

            return Scaffold(
              backgroundColor: BackGroundColor,
              body: ListView.builder(
                  controller: _scrollController,
                  shrinkWrap: true,
                  itemCount: (state.post != null && state.post.data != null)
                      ? state.post.data.length + 1
                      : 0,
                  itemBuilder: (BuildContext context, int index) =>
                      index == state.post.data.length
                          ? !state.isEmptyList
                              ? Center(
                                  child: Container(
                                  child: SpinKitThreeBounce(
                                    color: AmberColor,
                                    size: ScreenUtil().setHeight(50),
                                  ),
                                ))
                              : Container()
                          : GestureDetector(
                              child: myCard(state.post.data[index]),
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ArticleContent(
                                            postItem: state.post.data[index],
                                          )),
                                );
                              },
                            )),

              //  ListView.builder(
              //     shrinkWrap: true,
              //     itemCount: state.post != null ? state.post.data.length : 0,
              //     itemBuilder: (BuildContext context, int index) {
              //       return GestureDetector(
              //         child: myCard(state.post.data[index]),
              //         onTap: () {
              //           Navigator.push(
              //             context,
              //             MaterialPageRoute(
              //                 builder: (context) => ArticleContent(
              //                       postItem: state.post.data[index],
              //                     )),
              //           );
              //         },
              //       );
              //     }),
            );
          }),
    );
  }

  void error(String errorCode) {
    if (errorCode.isNotEmpty) {
      Fluttertoast.showToast(
          msg: ErrorMessage,
          //errorCode,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: AmberColor,
          textColor: Colors.white,
          fontSize: 16.0);
      _bloc.add(ClearError());
    }
  }

  Widget myCard(PostList item) {
    return Card(
      color: Colors.white,
      borderOnForeground: true,
      margin: EdgeInsets.only(
          top: ScreenUtil().setHeight(40.0),
          bottom: ScreenUtil().setHeight(20.0),
          right: ScreenUtil().setWidth(40.0),
          left: ScreenUtil().setWidth(40.0)),
      child: Column(
        //mainAxisAlignment: MainAxisAlignment.end,
//        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          firstSection(ImageUrl + item.image, item.user.name, item.created_at),
          secondSection(item),
        ],
      ),
    );
  }

  Widget firstSection(String baseImage, String publisher, String date) {
    return Stack(
      children: [
        Image.network(
          baseImage,
          width: MediaQuery.of(context).size.width,
          height: ScreenUtil().setHeight(425.0),
          fit: BoxFit.cover,
        ),
        Padding(
          padding: EdgeInsets.only(
              top: ScreenUtil().setHeight(360.0),
              bottom: ScreenUtil().setHeight(50.0),
              right: ScreenUtil().setHeight(20.0),
              left: ScreenUtil().setHeight(20.0)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Container(
                decoration: BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10),
                      bottomLeft: Radius.circular(10),
                      bottomRight: Radius.circular(10)
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 3,
                      blurRadius: 7,
                      offset: Offset(0, 1), // changes position of shadow
                    ),
                  ],
                ),
                child: Text(
                  date.substring(0, 10),
                  style: TextStyle(
                    fontFamily: 'Al-Jazeera-Arabic',
                    fontSize: ScreenUtil().setSp(40),
                    color: Colors.white,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10),
                      bottomLeft: Radius.circular(10),
                      bottomRight: Radius.circular(10)
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 3,
                      blurRadius: 7,
                      offset: Offset(0, 1), // changes position of shadow
                    ),
                  ],
                ),
                child: Text(
                  publisher,
                  style: TextStyle(
                    fontFamily: 'Al-Jazeera-Arabic',
                    fontSize: ScreenUtil().setSp(40),
                    color: Colors.white,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget secondSection(PostList item) {
    return Container(
      width: MediaQuery.of(context).size.width*0.8,
      padding:  EdgeInsets.only(
          top: ScreenUtil().setHeight(16.0),
          bottom: ScreenUtil().setHeight(16.0),
          right: ScreenUtil().setWidth(16.0),
          left: ScreenUtil().setWidth(16.0)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Text(
            item.title,
            style: TextStyle(
              fontFamily: 'Al-Jazeera-Arabic',
              fontSize: ScreenUtil().setSp(50),
              color: Colors.black,
              fontWeight: FontWeight.w700,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
         //   crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Flexible(
                child: Text(
                  item.description,
              //     softWrap: false,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.end,
                  style: TextStyle(
                    fontFamily: 'Al-Jazeera-Arabic',
                    fontSize: ScreenUtil().setSp(40),
                    color: Colors.black,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
