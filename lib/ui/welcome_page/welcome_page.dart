import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:pdfproject/constents/constent.dart';
import 'package:pdfproject/notification/push_notification.dart';
import 'package:pdfproject/ui/home_page/home_page.dart';
import 'package:url_launcher/url_launcher.dart';



class WelcomePage extends StatefulWidget {
  final bool launchUrl;


  const WelcomePage({Key key, this.launchUrl}) : super(key: key);

  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {

 // FirebaseMessaging _firebaseMessaging =  FirebaseMessaging();


  @override
  void initState() {
  //  _firebaseMessaging.subscribeToTopic("all");
    super.initState();
    Future.delayed(Duration(seconds: 5), () {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => HomePage()));
    });
    initPush();
  }

  initPush() async {
    PushNotificationManager fcm = PushNotificationManager();
    await fcm.init(context);
  }

  _launchURL() async {
    if (await canLaunch(LaunchedUrl)) {
      await launch(LaunchedUrl);
    } else {
      throw 'Could not launch $LaunchedUrl';
    }
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    if (widget.launchUrl) {
      _launchURL();
      return Container();
    }
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
          Image.asset(
            'assets/Images/splash.png',
            fit: BoxFit.cover,
          ),
        ]),
      ),
    );
  }
}
