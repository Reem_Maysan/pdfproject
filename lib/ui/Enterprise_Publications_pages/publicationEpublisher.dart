import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pdfproject/constents/constent.dart';
import 'package:pdfproject/injectoin.dart';
import 'package:pdfproject/models/publication_model/publication_list.dart';
import 'package:pdfproject/ui/Enterprise_Publications_pages/bloc/publications_bloc.dart';
import 'package:pdfproject/ui/Enterprise_Publications_pages/bloc/publications_event.dart';
import 'package:pdfproject/ui/Enterprise_Publications_pages/bloc/publications_state.dart';

class PublicationEpublisher extends StatefulWidget {
  @override
  _PublicationEpublisherState createState() => _PublicationEpublisherState();
}

class _PublicationEpublisherState extends State<PublicationEpublisher> {
  PDFDocument _pdfDocument;
  bool _isReadyPdf = false;
  final _bloc = sl<PublicationsBloc>();

  PublicationsState state;
  bool showProgressLoadMore = true;
  final _scrollController = ScrollController();
  final _scrollThreshold = 200.0;
  bool loadMoreRequest = false;

  DateTime currentBackPressTime;

  void _loadPdf(String url) async {
    setState(() {
      _isReadyPdf = false;
    });
    final pdf = await PDFDocument.fromURL(url);
    setState(() {
      _pdfDocument = pdf;
      _isReadyPdf = true;
    });
  }

  @override
  void initState() {
    _scrollController.addListener(_onScroll);
    _bloc.add(GetPublicationEpublisher((b) => b
      ..start_id = 0
      ..count = 10
      ..loadMoreRequest = false
      ..showProgress = true));
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll <= _scrollThreshold && !loadMoreRequest) {
      _loadMore();
    }
  }

  Future<bool> _loadMore() async {
    loadMoreRequest = true;
    print('-loadMore');
    if (state.publication != null && (state.isLoading || state.isEmptyList)) {
      return false;
    }
    if (state.publication != null) {
      _bloc.add(GetPublicationEpublisher((b) => b
        ..start_id = state.publication.publications
            .elementAt(state.publication.publications.length - 1)
            .id
        ..count = state.count
        ..showProgress = false
        ..loadMoreRequest = true));
      return true;
    }
  }

  Future<bool> _onWillPop() async {
    print('---------------------------------------------');
    print('publisher _onWillPop');
    print('---------------------------------------------');
    DateTime now = DateTime.now();
    if ((currentBackPressTime == null ||
        now.difference(currentBackPressTime) > Duration(seconds: 2))) {
      currentBackPressTime = now;
      return Future.value(false);
    } else {
      if (!_isReadyPdf)
        // SystemNavigator.pop();
        SystemChannels.platform.invokeMethod('SystemNavigator.pop');
      return Future.value(true);
    }
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    return WillPopScope(
      onWillPop: () {
        if (_isReadyPdf) {
          setState(() {
            _isReadyPdf = false;
          });
        }
        _onWillPop();
      },
      child: BlocBuilder(
          bloc: _bloc,
          builder: (BuildContext context, PublicationsState state) {
            this.state = state;
            this.loadMoreRequest = state.loadMoreRequest;
            error(state.error);
            if (state.isLoading || state.isLoadingPdf) {
              return Center(
                child: SpinKitThreeBounce(
                  color: AmberColor,
                  size: ScreenUtil().setHeight(50),
                ),
              );
            }
            return Scaffold(
              backgroundColor: BackGroundColor,
              body: _isReadyPdf
                  ? PDFViewer(
                      document: _pdfDocument,
                      indicatorBackground: Colors.amber,
                      indicatorText: Colors.black,
                      zoomSteps: 1,
                    )
                  : ListView.builder(
                      controller: _scrollController,
                      padding: EdgeInsets.only(
                        top: ScreenUtil().setHeight(40),
                      ),
                      shrinkWrap: true,
                      itemCount: (state.publication != null &&
                              state.publication.publications != null)
                          ? state.publication.publications.length + 1
                          : 0,
                      itemBuilder: (BuildContext context, int index) => index ==
                              state.publication.publications.length
                          ? !state.isEmptyList
                              ? Center(
                                  child: Container(
                                  child: SpinKitThreeBounce(
                                    color: AmberColor,
                                    size: ScreenUtil().setHeight(50),
                                  ),
                                ))
                              : Container()
                          : Column(
                              children: [
                                Theme(
                                  data: Theme.of(context).copyWith(
                                      dividerColor: Colors.transparent),
                                  child: ExpansionTile(
                                    initiallyExpanded: state.publication
                                        .publications[index].isExpanded,
                                    onExpansionChanged: (expanded) {
                                      print(
                                          'ExpansionTile pressed!! expanded is:' +
                                              expanded.toString());
                                      _bloc.add(GetPublicationIcon((b) => b
                                        ..isExpandedIcon = expanded
                                        ..indexIcon = state.publication
                                            .publications[index].id));
                                    },
                                    tilePadding: EdgeInsets.symmetric(
                                        horizontal:
                                            ScreenUtil().setHeight(20.0)),
                                    title: Text(
                                      state
                                          .publication.publications[index].name,
                                      textAlign: TextAlign.right,
                                      style: TextStyle(
                                        fontFamily: 'Al-Jazeera-Arabic',
                                        fontSize: ScreenUtil().setSp(45),
                                        color: BlackColor,
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                    trailing: IgnorePointer(
                                      child: Image.asset(
                                        state.publication.publications[index]
                                                .isExpanded
                                            ? PressedPublicationButton
                                            : UnPressedPublicationButton,
                                        fit: BoxFit.contain,
                                        height: ScreenUtil().setHeight(90),
                                        width: ScreenUtil().setWidth(90),
                                      ),
                                    ),
                                    children: <Widget>[
                                      cardWidget(state
                                          .publication.publications[index]),
                                    ],
                                  ),
                                ),
                                state.publication.publications.length - 1 !=
                                        index
                                    ? Divider(
                                        color: BlackColor,
                                        indent: ScreenUtil().setWidth(50.0),
                                        endIndent: ScreenUtil().setWidth(50.0),
                                      )
                                    : Container()
                              ],
                            )),
            );
          }),
    );
  }

  Widget cardWidget(PublicationList publication) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        ButtonTheme(
          minWidth: MediaQuery.of(context).size.width / 3,
          padding: EdgeInsets.only(
            top: ScreenUtil().setHeight(10),
            bottom: ScreenUtil().setHeight(10),
            right: ScreenUtil().setWidth(60),
            left: ScreenUtil().setWidth(60),
          ),
          child: RaisedButton(
              textColor: BlackColor,
              color: GrayColor,
              highlightColor: Colors.amber,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30.0))),
              onPressed: () {
                setState(() {
                  _loadPdf(PdfUrl + publication.file);
                });
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'استعراض',
                    style: TextStyle(
                      fontFamily: 'Al-Jazeera-Arabic',
                      fontSize: ScreenUtil().setSp(40),
                      //  color: const Color(0xff363638),
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  SizedBox(
                    width: ScreenUtil().setWidth(40.0),
                  ),
                  Image.asset(
                    ViewButton,
                    fit: BoxFit.contain,
                    height: ScreenUtil().setHeight(40),
                    width: ScreenUtil().setWidth(40),
                  ),
                ],
              )),
        ),
        SizedBox(
          width: ScreenUtil().setWidth(30.0),
        ),
        ButtonTheme(
          minWidth: MediaQuery.of(context).size.width / 3,
          padding: EdgeInsets.only(
            top: ScreenUtil().setHeight(10),
            bottom: ScreenUtil().setHeight(10),
            right: ScreenUtil().setWidth(90),
            left: ScreenUtil().setWidth(90),
          ),
          child: RaisedButton(
              textColor: BlackColor,
              color: AmberColor,
              highlightColor: Colors.amber,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30.0))),
              onPressed: () {
                setState(() {
                  _bloc.add(
                      GetPdf((b) => b..pdfPath = PdfUrl + publication.file));
                });
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'تحميل',
                    style: TextStyle(
                      fontFamily: 'Al-Jazeera-Arabic',
                      fontSize: ScreenUtil().setSp(40),
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  SizedBox(
                    width: ScreenUtil().setWidth(20.0),
                  ),
                  Image.asset(
                    DownloadButton,
                    fit: BoxFit.contain,
                    height: ScreenUtil().setHeight(40),
                    width: ScreenUtil().setWidth(40),
                  ),
                ],
              )),
        ),
      ],
    );
  }

  void error(String errorCode) {
    if (errorCode.isNotEmpty) {
      Fluttertoast.showToast(
          msg: ErrorMessage,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: AmberColor,
          textColor: Colors.white,
          fontSize: 16.0);
      _bloc.add(ClearError());
    }
  }
}
