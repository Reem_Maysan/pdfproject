import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pdfproject/data/repository/irepository.dart';
import 'package:pdfproject/ui/Enterprise_Publications_pages/bloc/publications_event.dart';
import 'package:pdfproject/ui/Enterprise_Publications_pages/bloc/publications_state.dart';

class PublicationsBloc extends Bloc<PublicationsEvent, PublicationsState> {
  IRepository _repository;

  PublicationsBloc(this._repository);

  @override
  PublicationsState get initialState => PublicationsState.initail();

  @override
  Stream<PublicationsState> mapEventToState(PublicationsEvent event) async* {
    if (event is ClearError) {
      yield state.rebuild((b) => b..error = "");
    }
    if (event is GetPublicationEpublisher && !state.isEmptyList) {
      try {
        yield state.rebuild((b) => b
          ..isLoading = event.showProgress
          ..error = "");
        final data = await _repository.getPublicationEpublisher(
            event.start_id, event.count);
        yield data.publications.isEmpty
            ? state.rebuild((b) => b..isEmptyList = true)
            : state.rebuild((b) => b
              ..isLoading = false
              ..error = ""
              ..count = event.count
              ..start_id = event.start_id
              ..loadMoreRequest = false
              ..publication.update((b) {
                if (!state.isEmptyList) {
                  b..publications.addAll(data.publications);
                  b
                    ..publications
                        .map((f) => f.rebuild((b) => b..isExpanded = false));
                }
              })
              ..isEmptyList = data.publications.isEmpty);
      } catch (e) {
        print('GetPublications Error $e');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = "Something went wrong");
      }
    }
    if (event is GetPublicationEbook && !state.isEmptyList) {
      try {
        yield state.rebuild((b) => b
          ..isLoading = event.showProgress
          ..error = "");
        final data =
            await _repository.getPublicationEbook(event.start_id, event.count);
        yield data.publications.isEmpty
            ? state.rebuild((b) => b..isEmptyList = true)
            : state.rebuild((b) => b
              ..isLoading = false
              ..error = ""
              ..count = event.count
              ..start_id = event.start_id
              ..loadMoreRequest = false
              ..publication.update((b) {
                if (!state.isEmptyList) {
                  b..publications.addAll(data.publications);
                  b
                    ..publications
                        .map((f) => f.rebuild((b) => b..isExpanded = false));
                }
              })
              ..isEmptyList = data.publications.isEmpty);
      } catch (e) {
        print('GetPublications Error $e');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = "Something went wrong");
      }
    }
    if (event is GetPublicationEmagazine && !state.isEmptyList) {
      try {
        yield state.rebuild((b) => b
          ..isLoading = event.showProgress
          ..error = "");
        final data = await _repository.getPublicationEmagazine(
            event.start_id, event.count);
        yield data.publications.isEmpty
            ? state.rebuild((b) => b..isEmptyList = true)
            : state.rebuild((b) => b
              ..isLoading = false
              ..error = ""
              ..count = event.count
              ..loadMoreRequest = false
              ..start_id = event.start_id
              ..publication.update((b) {
                if (!state.isEmptyList) {
                  b..publications.addAll(data.publications);
                  b
                    ..publications
                        .map((f) => f.rebuild((b) => b..isExpanded = false));
                }
              })
              ..isEmptyList = data.publications.isEmpty);
      } catch (e) {
        print('GetPublications Error $e');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = "Something went wrong");
      }
    }
    if (event is GetPublicationIcon) {
      try {
        yield state.rebuild((b) => b
          ..publication.update((b) {
            if (!state.isEmptyList) {
              b
                ..publications.map((f) {
                  if (f.id == event.indexIcon) {
                    print('weeee fouuund it!! its: ' +
                        f.id.toString() +
                        'b..isExpanded : ' +
                        f.isExpanded.toString() +
                        ' become :' +
                        event.isExpandedIcon.toString());
                    return f
                        .rebuild((b) => b..isExpanded = event.isExpandedIcon);
                  } else
                    return f.rebuild((b) => b..isExpanded = b.isExpanded);
                });
            }
          }));
      } catch (e) {
        print('GetPublicationIcon Error $e');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = "Something went wrong");
      }
    }
    if (event is GetPdf) {
      try {
        yield state.rebuild((b) => b..isLoadingPdf = true);
        final data = await _repository.getPdf(event.pdfPath);
        yield state.rebuild((b) => b..isLoadingPdf = false);
        print('GetPdf response $data');
      } catch (e) {
        print('GetPdf Error $e');
        yield state.rebuild((b) => b..error = "Something went wrong");
      }
    }
  }
}
