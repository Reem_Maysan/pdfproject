library publications_state;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:pdfproject/models/publication_model/publication_model.dart';

part 'publications_state.g.dart';

abstract class PublicationsState
    implements Built<PublicationsState, PublicationsStateBuilder> {
  // fields go here

  PublicationsState._();

  String get error;

  bool get isLoading;

  bool get isLoadingPdf;

  @nullable
  PublicationModel get publication;

  int get count;

  int get start_id;

  bool get isEmptyList;

  bool get isExpanded;

  bool get loadMoreRequest;
  factory PublicationsState([updates(PublicationsStateBuilder b)]) =
      _$PublicationsState;

  factory PublicationsState.initail() {
    return PublicationsState((b) => b
      ..error = ""
      ..isLoading = false
      ..publication = null
      ..count = 2
      ..start_id = 0
      ..isEmptyList = false
      ..isExpanded= false
      ..loadMoreRequest = false
      ..isLoadingPdf=false);
  }
}
