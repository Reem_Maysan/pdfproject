// GENERATED CODE - DO NOT MODIFY BY HAND

part of publications_state;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$PublicationsState extends PublicationsState {
  @override
  final String error;
  @override
  final bool isLoading;
  @override
  final bool isLoadingPdf;
  @override
  final PublicationModel publication;
  @override
  final int count;
  @override
  final int start_id;
  @override
  final bool isEmptyList;
  @override
  final bool isExpanded;
  @override
  final bool loadMoreRequest;

  factory _$PublicationsState(
          [void Function(PublicationsStateBuilder) updates]) =>
      (new PublicationsStateBuilder()..update(updates)).build();

  _$PublicationsState._(
      {this.error,
      this.isLoading,
      this.isLoadingPdf,
      this.publication,
      this.count,
      this.start_id,
      this.isEmptyList,
      this.isExpanded,
      this.loadMoreRequest})
      : super._() {
    if (error == null) {
      throw new BuiltValueNullFieldError('PublicationsState', 'error');
    }
    if (isLoading == null) {
      throw new BuiltValueNullFieldError('PublicationsState', 'isLoading');
    }
    if (isLoadingPdf == null) {
      throw new BuiltValueNullFieldError('PublicationsState', 'isLoadingPdf');
    }
    if (count == null) {
      throw new BuiltValueNullFieldError('PublicationsState', 'count');
    }
    if (start_id == null) {
      throw new BuiltValueNullFieldError('PublicationsState', 'start_id');
    }
    if (isEmptyList == null) {
      throw new BuiltValueNullFieldError('PublicationsState', 'isEmptyList');
    }
    if (isExpanded == null) {
      throw new BuiltValueNullFieldError('PublicationsState', 'isExpanded');
    }
    if (loadMoreRequest == null) {
      throw new BuiltValueNullFieldError(
          'PublicationsState', 'loadMoreRequest');
    }
  }

  @override
  PublicationsState rebuild(void Function(PublicationsStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PublicationsStateBuilder toBuilder() =>
      new PublicationsStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PublicationsState &&
        error == other.error &&
        isLoading == other.isLoading &&
        isLoadingPdf == other.isLoadingPdf &&
        publication == other.publication &&
        count == other.count &&
        start_id == other.start_id &&
        isEmptyList == other.isEmptyList &&
        isExpanded == other.isExpanded &&
        loadMoreRequest == other.loadMoreRequest;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc($jc($jc(0, error.hashCode), isLoading.hashCode),
                                isLoadingPdf.hashCode),
                            publication.hashCode),
                        count.hashCode),
                    start_id.hashCode),
                isEmptyList.hashCode),
            isExpanded.hashCode),
        loadMoreRequest.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PublicationsState')
          ..add('error', error)
          ..add('isLoading', isLoading)
          ..add('isLoadingPdf', isLoadingPdf)
          ..add('publication', publication)
          ..add('count', count)
          ..add('start_id', start_id)
          ..add('isEmptyList', isEmptyList)
          ..add('isExpanded', isExpanded)
          ..add('loadMoreRequest', loadMoreRequest))
        .toString();
  }
}

class PublicationsStateBuilder
    implements Builder<PublicationsState, PublicationsStateBuilder> {
  _$PublicationsState _$v;

  String _error;
  String get error => _$this._error;
  set error(String error) => _$this._error = error;

  bool _isLoading;
  bool get isLoading => _$this._isLoading;
  set isLoading(bool isLoading) => _$this._isLoading = isLoading;

  bool _isLoadingPdf;
  bool get isLoadingPdf => _$this._isLoadingPdf;
  set isLoadingPdf(bool isLoadingPdf) => _$this._isLoadingPdf = isLoadingPdf;

  PublicationModelBuilder _publication;
  PublicationModelBuilder get publication =>
      _$this._publication ??= new PublicationModelBuilder();
  set publication(PublicationModelBuilder publication) =>
      _$this._publication = publication;

  int _count;
  int get count => _$this._count;
  set count(int count) => _$this._count = count;

  int _start_id;
  int get start_id => _$this._start_id;
  set start_id(int start_id) => _$this._start_id = start_id;

  bool _isEmptyList;
  bool get isEmptyList => _$this._isEmptyList;
  set isEmptyList(bool isEmptyList) => _$this._isEmptyList = isEmptyList;

  bool _isExpanded;
  bool get isExpanded => _$this._isExpanded;
  set isExpanded(bool isExpanded) => _$this._isExpanded = isExpanded;

  bool _loadMoreRequest;
  bool get loadMoreRequest => _$this._loadMoreRequest;
  set loadMoreRequest(bool loadMoreRequest) =>
      _$this._loadMoreRequest = loadMoreRequest;

  PublicationsStateBuilder();

  PublicationsStateBuilder get _$this {
    if (_$v != null) {
      _error = _$v.error;
      _isLoading = _$v.isLoading;
      _isLoadingPdf = _$v.isLoadingPdf;
      _publication = _$v.publication?.toBuilder();
      _count = _$v.count;
      _start_id = _$v.start_id;
      _isEmptyList = _$v.isEmptyList;
      _isExpanded = _$v.isExpanded;
      _loadMoreRequest = _$v.loadMoreRequest;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PublicationsState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$PublicationsState;
  }

  @override
  void update(void Function(PublicationsStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$PublicationsState build() {
    _$PublicationsState _$result;
    try {
      _$result = _$v ??
          new _$PublicationsState._(
              error: error,
              isLoading: isLoading,
              isLoadingPdf: isLoadingPdf,
              publication: _publication?.build(),
              count: count,
              start_id: start_id,
              isEmptyList: isEmptyList,
              isExpanded: isExpanded,
              loadMoreRequest: loadMoreRequest);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'publication';
        _publication?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'PublicationsState', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
