library publications_event;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'publications_event.g.dart';

abstract class PublicationsEvent {}

abstract class ClearError extends PublicationsEvent
    implements Built<ClearError, ClearErrorBuilder> {
  // fields go here

  ClearError._();

  factory ClearError([updates(ClearErrorBuilder b)]) = _$ClearError;
}


abstract class GetPublicationEbook extends PublicationsEvent
    implements Built<GetPublicationEbook, GetPublicationEbookBuilder> {
  // fields go here

  int get count;

  int get start_id;

  bool get showProgress;

  bool get loadMoreRequest;
  GetPublicationEbook._();

  factory GetPublicationEbook([updates(GetPublicationEbookBuilder b)]) = _$GetPublicationEbook;
}

abstract class GetPublicationEpublisher extends PublicationsEvent
    implements Built<GetPublicationEpublisher, GetPublicationEpublisherBuilder> {
  // fields go here

  int get count;

  int get start_id;

  bool get showProgress;

  bool get loadMoreRequest;
  GetPublicationEpublisher._();

  factory GetPublicationEpublisher([updates(GetPublicationEpublisherBuilder b)]) = _$GetPublicationEpublisher;
}

abstract class GetPublicationEmagazine extends PublicationsEvent
    implements Built<GetPublicationEmagazine, GetPublicationEmagazineBuilder> {
  // fields go here

  int get count;

  int get start_id;

  bool get showProgress;

  bool get loadMoreRequest;
  GetPublicationEmagazine._();

  factory GetPublicationEmagazine([updates(GetPublicationEmagazineBuilder b)]) = _$GetPublicationEmagazine;
}

abstract class GetPublicationIcon extends PublicationsEvent
    implements Built<GetPublicationIcon, GetPublicationIconBuilder> {
  // fields go here

  bool get isExpandedIcon;
  int get indexIcon;

  GetPublicationIcon._();

  factory GetPublicationIcon([updates(GetPublicationIconBuilder b)]) = _$GetPublicationIcon;
}

abstract class GetPdf extends PublicationsEvent
    implements Built<GetPdf, GetPdfBuilder> {
  // fields go here

  String get pdfPath;

  GetPdf._();

  factory GetPdf([updates(GetPdfBuilder b)]) = _$GetPdf;
}
