// GENERATED CODE - DO NOT MODIFY BY HAND

part of publications_event;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ClearError extends ClearError {
  factory _$ClearError([void Function(ClearErrorBuilder) updates]) =>
      (new ClearErrorBuilder()..update(updates)).build();

  _$ClearError._() : super._();

  @override
  ClearError rebuild(void Function(ClearErrorBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ClearErrorBuilder toBuilder() => new ClearErrorBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ClearError;
  }

  @override
  int get hashCode {
    return 507656265;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('ClearError').toString();
  }
}

class ClearErrorBuilder implements Builder<ClearError, ClearErrorBuilder> {
  _$ClearError _$v;

  ClearErrorBuilder();

  @override
  void replace(ClearError other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ClearError;
  }

  @override
  void update(void Function(ClearErrorBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ClearError build() {
    final _$result = _$v ?? new _$ClearError._();
    replace(_$result);
    return _$result;
  }
}

class _$GetPublicationEbook extends GetPublicationEbook {
  @override
  final int count;
  @override
  final int start_id;
  @override
  final bool showProgress;
  @override
  final bool loadMoreRequest;

  factory _$GetPublicationEbook(
          [void Function(GetPublicationEbookBuilder) updates]) =>
      (new GetPublicationEbookBuilder()..update(updates)).build();

  _$GetPublicationEbook._(
      {this.count, this.start_id, this.showProgress, this.loadMoreRequest})
      : super._() {
    if (count == null) {
      throw new BuiltValueNullFieldError('GetPublicationEbook', 'count');
    }
    if (start_id == null) {
      throw new BuiltValueNullFieldError('GetPublicationEbook', 'start_id');
    }
    if (showProgress == null) {
      throw new BuiltValueNullFieldError('GetPublicationEbook', 'showProgress');
    }
    if (loadMoreRequest == null) {
      throw new BuiltValueNullFieldError(
          'GetPublicationEbook', 'loadMoreRequest');
    }
  }

  @override
  GetPublicationEbook rebuild(
          void Function(GetPublicationEbookBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetPublicationEbookBuilder toBuilder() =>
      new GetPublicationEbookBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetPublicationEbook &&
        count == other.count &&
        start_id == other.start_id &&
        showProgress == other.showProgress &&
        loadMoreRequest == other.loadMoreRequest;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, count.hashCode), start_id.hashCode),
            showProgress.hashCode),
        loadMoreRequest.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GetPublicationEbook')
          ..add('count', count)
          ..add('start_id', start_id)
          ..add('showProgress', showProgress)
          ..add('loadMoreRequest', loadMoreRequest))
        .toString();
  }
}

class GetPublicationEbookBuilder
    implements Builder<GetPublicationEbook, GetPublicationEbookBuilder> {
  _$GetPublicationEbook _$v;

  int _count;
  int get count => _$this._count;
  set count(int count) => _$this._count = count;

  int _start_id;
  int get start_id => _$this._start_id;
  set start_id(int start_id) => _$this._start_id = start_id;

  bool _showProgress;
  bool get showProgress => _$this._showProgress;
  set showProgress(bool showProgress) => _$this._showProgress = showProgress;

  bool _loadMoreRequest;
  bool get loadMoreRequest => _$this._loadMoreRequest;
  set loadMoreRequest(bool loadMoreRequest) =>
      _$this._loadMoreRequest = loadMoreRequest;

  GetPublicationEbookBuilder();

  GetPublicationEbookBuilder get _$this {
    if (_$v != null) {
      _count = _$v.count;
      _start_id = _$v.start_id;
      _showProgress = _$v.showProgress;
      _loadMoreRequest = _$v.loadMoreRequest;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetPublicationEbook other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetPublicationEbook;
  }

  @override
  void update(void Function(GetPublicationEbookBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetPublicationEbook build() {
    final _$result = _$v ??
        new _$GetPublicationEbook._(
            count: count,
            start_id: start_id,
            showProgress: showProgress,
            loadMoreRequest: loadMoreRequest);
    replace(_$result);
    return _$result;
  }
}

class _$GetPublicationEpublisher extends GetPublicationEpublisher {
  @override
  final int count;
  @override
  final int start_id;
  @override
  final bool showProgress;
  @override
  final bool loadMoreRequest;

  factory _$GetPublicationEpublisher(
          [void Function(GetPublicationEpublisherBuilder) updates]) =>
      (new GetPublicationEpublisherBuilder()..update(updates)).build();

  _$GetPublicationEpublisher._(
      {this.count, this.start_id, this.showProgress, this.loadMoreRequest})
      : super._() {
    if (count == null) {
      throw new BuiltValueNullFieldError('GetPublicationEpublisher', 'count');
    }
    if (start_id == null) {
      throw new BuiltValueNullFieldError(
          'GetPublicationEpublisher', 'start_id');
    }
    if (showProgress == null) {
      throw new BuiltValueNullFieldError(
          'GetPublicationEpublisher', 'showProgress');
    }
    if (loadMoreRequest == null) {
      throw new BuiltValueNullFieldError(
          'GetPublicationEpublisher', 'loadMoreRequest');
    }
  }

  @override
  GetPublicationEpublisher rebuild(
          void Function(GetPublicationEpublisherBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetPublicationEpublisherBuilder toBuilder() =>
      new GetPublicationEpublisherBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetPublicationEpublisher &&
        count == other.count &&
        start_id == other.start_id &&
        showProgress == other.showProgress &&
        loadMoreRequest == other.loadMoreRequest;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, count.hashCode), start_id.hashCode),
            showProgress.hashCode),
        loadMoreRequest.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GetPublicationEpublisher')
          ..add('count', count)
          ..add('start_id', start_id)
          ..add('showProgress', showProgress)
          ..add('loadMoreRequest', loadMoreRequest))
        .toString();
  }
}

class GetPublicationEpublisherBuilder
    implements
        Builder<GetPublicationEpublisher, GetPublicationEpublisherBuilder> {
  _$GetPublicationEpublisher _$v;

  int _count;
  int get count => _$this._count;
  set count(int count) => _$this._count = count;

  int _start_id;
  int get start_id => _$this._start_id;
  set start_id(int start_id) => _$this._start_id = start_id;

  bool _showProgress;
  bool get showProgress => _$this._showProgress;
  set showProgress(bool showProgress) => _$this._showProgress = showProgress;

  bool _loadMoreRequest;
  bool get loadMoreRequest => _$this._loadMoreRequest;
  set loadMoreRequest(bool loadMoreRequest) =>
      _$this._loadMoreRequest = loadMoreRequest;

  GetPublicationEpublisherBuilder();

  GetPublicationEpublisherBuilder get _$this {
    if (_$v != null) {
      _count = _$v.count;
      _start_id = _$v.start_id;
      _showProgress = _$v.showProgress;
      _loadMoreRequest = _$v.loadMoreRequest;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetPublicationEpublisher other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetPublicationEpublisher;
  }

  @override
  void update(void Function(GetPublicationEpublisherBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetPublicationEpublisher build() {
    final _$result = _$v ??
        new _$GetPublicationEpublisher._(
            count: count,
            start_id: start_id,
            showProgress: showProgress,
            loadMoreRequest: loadMoreRequest);
    replace(_$result);
    return _$result;
  }
}

class _$GetPublicationEmagazine extends GetPublicationEmagazine {
  @override
  final int count;
  @override
  final int start_id;
  @override
  final bool showProgress;
  @override
  final bool loadMoreRequest;

  factory _$GetPublicationEmagazine(
          [void Function(GetPublicationEmagazineBuilder) updates]) =>
      (new GetPublicationEmagazineBuilder()..update(updates)).build();

  _$GetPublicationEmagazine._(
      {this.count, this.start_id, this.showProgress, this.loadMoreRequest})
      : super._() {
    if (count == null) {
      throw new BuiltValueNullFieldError('GetPublicationEmagazine', 'count');
    }
    if (start_id == null) {
      throw new BuiltValueNullFieldError('GetPublicationEmagazine', 'start_id');
    }
    if (showProgress == null) {
      throw new BuiltValueNullFieldError(
          'GetPublicationEmagazine', 'showProgress');
    }
    if (loadMoreRequest == null) {
      throw new BuiltValueNullFieldError(
          'GetPublicationEmagazine', 'loadMoreRequest');
    }
  }

  @override
  GetPublicationEmagazine rebuild(
          void Function(GetPublicationEmagazineBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetPublicationEmagazineBuilder toBuilder() =>
      new GetPublicationEmagazineBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetPublicationEmagazine &&
        count == other.count &&
        start_id == other.start_id &&
        showProgress == other.showProgress &&
        loadMoreRequest == other.loadMoreRequest;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, count.hashCode), start_id.hashCode),
            showProgress.hashCode),
        loadMoreRequest.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GetPublicationEmagazine')
          ..add('count', count)
          ..add('start_id', start_id)
          ..add('showProgress', showProgress)
          ..add('loadMoreRequest', loadMoreRequest))
        .toString();
  }
}

class GetPublicationEmagazineBuilder
    implements
        Builder<GetPublicationEmagazine, GetPublicationEmagazineBuilder> {
  _$GetPublicationEmagazine _$v;

  int _count;
  int get count => _$this._count;
  set count(int count) => _$this._count = count;

  int _start_id;
  int get start_id => _$this._start_id;
  set start_id(int start_id) => _$this._start_id = start_id;

  bool _showProgress;
  bool get showProgress => _$this._showProgress;
  set showProgress(bool showProgress) => _$this._showProgress = showProgress;

  bool _loadMoreRequest;
  bool get loadMoreRequest => _$this._loadMoreRequest;
  set loadMoreRequest(bool loadMoreRequest) =>
      _$this._loadMoreRequest = loadMoreRequest;

  GetPublicationEmagazineBuilder();

  GetPublicationEmagazineBuilder get _$this {
    if (_$v != null) {
      _count = _$v.count;
      _start_id = _$v.start_id;
      _showProgress = _$v.showProgress;
      _loadMoreRequest = _$v.loadMoreRequest;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetPublicationEmagazine other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetPublicationEmagazine;
  }

  @override
  void update(void Function(GetPublicationEmagazineBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetPublicationEmagazine build() {
    final _$result = _$v ??
        new _$GetPublicationEmagazine._(
            count: count,
            start_id: start_id,
            showProgress: showProgress,
            loadMoreRequest: loadMoreRequest);
    replace(_$result);
    return _$result;
  }
}

class _$GetPublicationIcon extends GetPublicationIcon {
  @override
  final bool isExpandedIcon;
  @override
  final int indexIcon;

  factory _$GetPublicationIcon(
          [void Function(GetPublicationIconBuilder) updates]) =>
      (new GetPublicationIconBuilder()..update(updates)).build();

  _$GetPublicationIcon._({this.isExpandedIcon, this.indexIcon}) : super._() {
    if (isExpandedIcon == null) {
      throw new BuiltValueNullFieldError(
          'GetPublicationIcon', 'isExpandedIcon');
    }
    if (indexIcon == null) {
      throw new BuiltValueNullFieldError('GetPublicationIcon', 'indexIcon');
    }
  }

  @override
  GetPublicationIcon rebuild(
          void Function(GetPublicationIconBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetPublicationIconBuilder toBuilder() =>
      new GetPublicationIconBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetPublicationIcon &&
        isExpandedIcon == other.isExpandedIcon &&
        indexIcon == other.indexIcon;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, isExpandedIcon.hashCode), indexIcon.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GetPublicationIcon')
          ..add('isExpandedIcon', isExpandedIcon)
          ..add('indexIcon', indexIcon))
        .toString();
  }
}

class GetPublicationIconBuilder
    implements Builder<GetPublicationIcon, GetPublicationIconBuilder> {
  _$GetPublicationIcon _$v;

  bool _isExpandedIcon;
  bool get isExpandedIcon => _$this._isExpandedIcon;
  set isExpandedIcon(bool isExpandedIcon) =>
      _$this._isExpandedIcon = isExpandedIcon;

  int _indexIcon;
  int get indexIcon => _$this._indexIcon;
  set indexIcon(int indexIcon) => _$this._indexIcon = indexIcon;

  GetPublicationIconBuilder();

  GetPublicationIconBuilder get _$this {
    if (_$v != null) {
      _isExpandedIcon = _$v.isExpandedIcon;
      _indexIcon = _$v.indexIcon;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetPublicationIcon other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetPublicationIcon;
  }

  @override
  void update(void Function(GetPublicationIconBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetPublicationIcon build() {
    final _$result = _$v ??
        new _$GetPublicationIcon._(
            isExpandedIcon: isExpandedIcon, indexIcon: indexIcon);
    replace(_$result);
    return _$result;
  }
}

class _$GetPdf extends GetPdf {
  @override
  final String pdfPath;

  factory _$GetPdf([void Function(GetPdfBuilder) updates]) =>
      (new GetPdfBuilder()..update(updates)).build();

  _$GetPdf._({this.pdfPath}) : super._() {
    if (pdfPath == null) {
      throw new BuiltValueNullFieldError('GetPdf', 'pdfPath');
    }
  }

  @override
  GetPdf rebuild(void Function(GetPdfBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetPdfBuilder toBuilder() => new GetPdfBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetPdf && pdfPath == other.pdfPath;
  }

  @override
  int get hashCode {
    return $jf($jc(0, pdfPath.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('GetPdf')..add('pdfPath', pdfPath))
        .toString();
  }
}

class GetPdfBuilder implements Builder<GetPdf, GetPdfBuilder> {
  _$GetPdf _$v;

  String _pdfPath;
  String get pdfPath => _$this._pdfPath;
  set pdfPath(String pdfPath) => _$this._pdfPath = pdfPath;

  GetPdfBuilder();

  GetPdfBuilder get _$this {
    if (_$v != null) {
      _pdfPath = _$v.pdfPath;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetPdf other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$GetPdf;
  }

  @override
  void update(void Function(GetPdfBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$GetPdf build() {
    final _$result = _$v ?? new _$GetPdf._(pdfPath: pdfPath);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
