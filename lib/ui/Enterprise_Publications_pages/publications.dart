import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pdfproject/constents/constent.dart';
import 'package:pdfproject/ui/Enterprise_Publications_pages/publicationEbook.dart';
import 'package:pdfproject/ui/Enterprise_Publications_pages/publicationEmagazine.dart';
import 'package:pdfproject/ui/Enterprise_Publications_pages/publicationEpublisher.dart';

class PublicationsPage extends StatefulWidget {
  @override
  _PublicationsPageState createState() => _PublicationsPageState();
}

class _PublicationsPageState extends State<PublicationsPage>
    with SingleTickerProviderStateMixin {

  TabController _tabController;
  double _preferredHieght = 80.0;

  String pressedBookButton = 'assets/Icons/research-or.png';
  String unPressedBookButton = 'assets/Icons/research-black.png';

  @override
  void initState() {
    _tabController = new TabController(vsync: this, length: 3);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    return Scaffold(
      backgroundColor:BackGroundColor ,
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(_preferredHieght),
            child: Container(

                child: TabBar(
                    indicatorColor: AmberColor,
                    controller: _tabController, tabs: [
              new Tab(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      'مجلات',
                      style: TextStyle(
                        fontFamily: 'Al-Jazeera-Arabic',
                        fontSize: ScreenUtil().setSp(50),
                        color: Colors.black,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    SizedBox(
                      width: ScreenUtil().setWidth(30.0),
                    ),
                    Image.asset(
                      unPressedBookButton,
                      fit: BoxFit.contain,
                      height: ScreenUtil().setHeight(60),
                      width: ScreenUtil().setWidth(60),
                    ),
                  ],
                ),
              ),
              new Tab(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      'نشرات',
                      style: TextStyle(
                        fontFamily: 'Al-Jazeera-Arabic',
                        fontSize: ScreenUtil().setSp(50),
                        color: Colors.black,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    SizedBox(
                      width: ScreenUtil().setWidth(30.0),
                    ),
                    Image.asset(
                      unPressedBookButton,
                      fit: BoxFit.contain,
                      height: ScreenUtil().setHeight(60),
                      width: ScreenUtil().setWidth(60),
                    ),
                  ],
                ),
              ),
              new Tab(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      'كتب',
                      style: TextStyle(
                        fontFamily: 'Al-Jazeera-Arabic',
                        fontSize: ScreenUtil().setSp(50),
                        color: Colors.black,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    SizedBox(
                      width: ScreenUtil().setWidth(30.0),
                    ),
                    Image.asset(
                      unPressedBookButton,
                      fit: BoxFit.contain,
                      height: ScreenUtil().setHeight(60),
                      width: ScreenUtil().setWidth(60),
                    ),
                  ],
                ),
              ),
            ]))),
        body: TabBarView(
          controller: _tabController,
          children: <Widget>[
            PublicationEmagazine(),
            PublicationEpublisher(),
            PublicationEbook(),
          ],
        ));
  }
}
