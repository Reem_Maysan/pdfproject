/*

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:forsa/api/job_api.dart';
import 'package:forsa/localization/app_translation.dart';
import 'package:forsa/model/AllJobs.dart';
import 'package:forsa/model/Forsa.dart';
import 'package:forsa/screens/HomePage.dart';
import 'package:forsa/utils/BoxDecorationLayout.dart';
import 'package:forsa/utils/ColorFile.dart';
import 'package:forsa/utils/Constants.dart';
import 'package:forsa/utils/Dimens.dart';
import 'package:forsa/utils/MyTokenApi.dart';
import 'package:forsa/utils/Utils.dart';
import 'package:forsa/list_item_builders/ItemListJob.dart';
import 'package:forsa/viewmodels/job_view_model.dart';

class AllJobFragment extends StatefulWidget {
  final AllJobs allJobs;
  final HomePageState parent;

  AllJobFragment(this.parent, this.allJobs);

  @override
  State<StatefulWidget> createState() {
    return AllJobFragmentState(parent, allJobs);
  }
}

class AllJobFragmentState extends State<AllJobFragment> {
  AllJobs allJobs;
  Utils utils;
  MyBoxDecoration myBoxDecoration;
  HomePageState parent;

  AllJobFragmentState(this.parent, this.allJobs);

  TextEditingController searchController = TextEditingController();
  var _scaffoldKey = GlobalKey<ScaffoldState>();

  List<Forsa> foras = List();
  int page = 0;
  int totalPage = 1;
  String text;

  bool showProgress = false;
  bool showProgressLoadMore = true;
  bool showNoResult = false;

  @override
  void initState() {
    super.initState();

    searchController.addListener((){
      if(searchController.text == ' '){
        searchController.text = '';
      }
    });

    if(allJobs.allJobsList != null){
      foras.addAll(allJobs.allJobsList);
    }

    // those for no caching listView's items
    page = 0;
    totalPage = 1;
  }

  @override
  Widget build(BuildContext context) {
    utils = Utils.getInstance(context);
    myBoxDecoration = MyBoxDecoration.getInstance();

    return Directionality(
      textDirection:
          utils.isDirectionRTL(context) ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: ColorFile.grayColor,
        body: !showProgress
            ? Column(
                children: <Widget>[
                  searchLayout(),
                  !showNoResult
                      ? allJobs.allJobsList != null
                          ? NotificationListener<ScrollNotification>(
                              onNotification: (ScrollNotification scrollInfo) {
                                if (scrollInfo.metrics.pixels ==
                                    scrollInfo.metrics.maxScrollExtent) {
                                  Future.delayed(
                                      const Duration(milliseconds: 1000), () {
                                    setState(() {
                                      loadMore();
                                    });
                                  });
                                }
                                return true;
                              },
                              child: Expanded(
                                child: ListView.builder(
                                  padding: EdgeInsetsDirectional.only(
                                      top: Dimens.listForsaItemMargin),
                                  shrinkWrap: true,
                                  itemCount: foras.length + 1,
                                  itemBuilder: (BuildContext context,
                                          int index) =>
                                      index == foras.length
                                          ? showProgressLoadMore
                                              ? Center(
                                                  child: Container(
                                                  child: SpinKitThreeBounce(
                                                    color:
                                                        ColorFile.primaryColor,
                                                    size: 20,
                                                  ),
                                                ))
                                              : Container()
                                          : ItemListJob(
                                              item: foras[index],
                                              onClickForsaItem: (Forsa item) {
                                                Navigator.pushNamed(context,
                                                    Constants.FORSA_DETAILS,
                                                    arguments: item.forsaId);
                                              },
                                            ),
                                ),
                              ),
                            )
                          : Container()
                      : noResultImage()
                ],
              )
            : Container(
                color: Colors.transparent,
                child: Center(
                  child: SpinKitFadingCircle(
                    color: ColorFile.primaryColor,
                    size: 50.0,
                  ),
                ),
              ),
      ),
    );
  }

  Widget searchLayout() {
    return Container(
        alignment: Alignment.center,
        decoration: myBoxDecoration.boxDecorationSearchAllJobs(),
        margin: EdgeInsets.only(right: 20, left: 20, top: 20,bottom: 5),
        child: Row(
          children: <Widget>[
            Container(
              child: Image.asset(
                'assets/images/search_ic.png',
                fit: BoxFit.scaleDown,
                height: 15,
                width: 15,
              ),
              margin: EdgeInsetsDirectional.only(start: 8),
            ),
            Flexible(
              child: TextField(
                controller: searchController,
                textInputAction: TextInputAction.search,
                onSubmitted: (value) {
                  setState(() {
                    foras.clear();
                    text = value;
                    page = 0;
                    showProgressLoadMore = true;
                    filterApi(text, page.toString(), true);
                  });
                },
                keyboardType: TextInputType.text,
                maxLines: 1,
                style: TextStyle(fontSize: 14),
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(8),
                    hintText: AppTranslations.of(context).text('Search'),
                    isDense: true,
                    border: InputBorder.none,
                    hintStyle: TextStyle(fontSize: 13)),
              ),
            )
          ],
        ));
  }

  void loadMore() {
    setState(() {
      if (page < totalPage) {
        print('loadMore');
        showProgressLoadMore = true;
        page = page + 1;
        filterApi(text, page.toString(), false);
      } else {
        setState(() {
          showProgressLoadMore = false;
        });
      }
    });
  }

  filterApi(String text, String page, bool showIndicator) {
    JobViewModel _model = JobViewModel(api: JobApi());

    if (showIndicator) {
      setState(() {
        showProgress = true;
      });
    }

    _model.filterApi(context, text, page).then((filter) {
      switch (filter.statusCode) {
        case 200:
          setState(() {
            foras.addAll(filter.foras);
            print('size: ' + foras.length.toString());
            totalPage = int.parse(filter.totalPages);
            showProgress = false;
          });

          if (foras.isEmpty) {
            setState(() {
              showNoResult = true;
            });
          } else
            setState(() {
              showNoResult = false;
            });

          break;
        case 400:
          print('400');
          break;
        case 401:
          setState(() {
            showProgress = false;
          });
          MyTokenApi.getInstance(context)
              .getToken(_model.parentViewModel)
              .then((myToken) {
            switch (myToken.statusCode) {
              case 200:
                filterApi(text, page, true);
                break;
              case 1000:
                setState(() {
                  showProgress = false;
                });
                Utils.getInstance(context).showSnackbarConnection(_scaffoldKey);
                break;
            }
          });
          break;
        case 500:
          print('500 register employee');
          break;
        case 1000:
          setState(() {
            showProgress = false;
          });
          Utils.getInstance(context).showSnackbarConnection(_scaffoldKey);
          break;
      }
    });
  }

  Widget noResultImage() {
    return Expanded(
      child: Container(
        margin: EdgeInsets.only(top: 70),
        alignment: Alignment.center,
        child: Column(
          children: <Widget>[
            Flexible(
              child: Image.asset(
                'assets/images/no_result_img.png',
                width: 200,
                height: 200,
                fit: BoxFit.scaleDown,
              ),
            ),
            Flexible(
              child: Container(
                  padding: EdgeInsets.all(20),
                  child: Text(
                    AppTranslations.of(context).text('NoResultAllJob'),
                    style: TextStyle(fontSize: 15),
                    textAlign: TextAlign.center,
                  )),
            )
          ],
        ),
      ),
    );
  }
}

*/