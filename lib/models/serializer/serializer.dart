library serializer;

import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';

import 'package:built_collection/built_collection.dart';
import 'package:pdfproject/models/book_model/book_list.dart';
import 'package:pdfproject/models/book_model/book_model.dart';
import 'package:pdfproject/models/post_model/post_list.dart';
import 'package:pdfproject/models/post_model/post_model.dart';
import 'package:pdfproject/models/publication_model/publication_list.dart';
import 'package:pdfproject/models/publication_model/publication_model.dart';
import 'package:pdfproject/models/user_model/user_model.dart';
import 'package:pdfproject/models/research_model/research_model.dart';
import 'package:pdfproject/models/research_model/research_list.dart';

part 'serializer.g.dart';

@SerializersFor(const [
  UserModel,
  PostModel,
  ResearchModel,
  BookModel,
  ResearchModel,
  PublicationModel
])
final Serializers serializers = (_$serializers.toBuilder()
      ..addPlugin(StandardJsonPlugin())
      ..addBuilderFactory(
          (FullType(
            BuiltList,
            [
              const FullType(ResearchModel),
            ],
          )),
          () => ListBuilder<ResearchModel>())
      ..addBuilderFactory(
          (FullType(
            BuiltList,
            [
              const FullType(ResearchList),
            ],
          )),
          () => ListBuilder<ResearchList>())
      ..addBuilderFactory(
          (FullType(
            BuiltList,
            [
              const FullType(PostModel),
            ],
          )),
          () => ListBuilder<PostModel>())
      ..addBuilderFactory(
          (FullType(
            BuiltList,
            [
              const FullType(PostList),
            ],
          )),
          () => ListBuilder<PostList>())
      ..addBuilderFactory(
          (FullType(
            BuiltList,
            [
              const FullType(PublicationModel),
            ],
          )),
          () => ListBuilder<PublicationModel>())
      ..addBuilderFactory(
          (FullType(
            BuiltList,
            [
              const FullType(PublicationList),
            ],
          )),
          () => ListBuilder<PublicationList>())
      ..addBuilderFactory(
          (FullType(
            BuiltList,
            [
              const FullType(BookModel),
            ],
          )),
          () => ListBuilder<BookModel>())
      ..addBuilderFactory(
          (FullType(
            BuiltList,
            [
              const FullType(BookList),
            ],
          )),
          () => ListBuilder<BookList>()))
    .build();
