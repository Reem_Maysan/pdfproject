// GENERATED CODE - DO NOT MODIFY BY HAND

part of serializer;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializers _$serializers = (new Serializers().toBuilder()
      ..add(BookList.serializer)
      ..add(BookModel.serializer)
      ..add(PostList.serializer)
      ..add(PostModel.serializer)
      ..add(PublicationList.serializer)
      ..add(PublicationModel.serializer)
      ..add(ResearchList.serializer)
      ..add(ResearchModel.serializer)
      ..add(UserModel.serializer)
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(BookList)]),
          () => new ListBuilder<BookList>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(PostList)]),
          () => new ListBuilder<PostList>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(PublicationList)]),
          () => new ListBuilder<PublicationList>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(ResearchList)]),
          () => new ListBuilder<ResearchList>()))
    .build();

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
