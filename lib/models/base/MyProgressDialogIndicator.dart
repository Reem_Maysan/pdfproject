import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';


class MyProgressDialogIndicator extends StatelessWidget {
  final Widget child;
  final bool inAsyncCall;
  final double opacity;
  final Color color;
  final Color valueColor;

  MyProgressDialogIndicator({
    Key key,
    @required this.child,
    @required this.inAsyncCall,
    this.opacity = 0.3,
    this.color = Colors.black,
    this.valueColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> widgetList = new List<Widget>();
    widgetList.add(child);
    if (inAsyncCall) {
      final modal = new WillPopScope(
        onWillPop: () {},
          child: Stack(
            children: [
              new Opacity(
                opacity: opacity,
                child: ModalBarrier(dismissible: false, color: color),
              ),
              new Center(
                child: SpinKitDualRing(
                  color: Colors.white,
                ),
              ),
            ],
          ),
      );
      widgetList.add(modal);
    }
    return Stack(
      children: widgetList,
    );
  }
}
