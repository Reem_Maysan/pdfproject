import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:loadmore/loadmore.dart';

class MyLoadMoreDelegate extends LoadMoreDelegate {
  BuildContext context;

  MyLoadMoreDelegate({this.context});

  @override
  Duration loadMoreDelay() {
    return Duration(milliseconds: 300);
  }

  @override
  Widget buildChild(LoadMoreStatus status,
      {LoadMoreTextBuilder builder = DefaultLoadMoreTextBuilder.chinese}) {
    ScreenUtil.init(context);
    if (status == LoadMoreStatus.loading) {
      return Container(
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SpinKitThreeBounce(
              color: Colors.white,
              size: ScreenUtil().setWidth(70),
            ),
          ],
        ),
      );
    }
    return Container();
  }
}
