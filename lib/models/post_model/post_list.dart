library post_list;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:pdfproject/models/serializer/serializer.dart';
import 'package:pdfproject/models/user_model/user_model.dart';

part 'post_list.g.dart';

abstract class PostList implements Built<PostList, PostListBuilder> {
  // fields go here

  int get id;
  String get title;
  String get description;
  String get image;
  String get user_id;
  String get created_at;
  String get updated_at;
  UserModel get user;

  PostList._();

  factory PostList([updates(PostListBuilder b)]) = _$PostList;

  String toJson() {
    return json.encode(serializers.serializeWith(PostList.serializer, this));
  }

  static PostList fromJson(String jsonString) {
    return serializers.deserializeWith(PostList.serializer, json.decode(jsonString));
  }

  static Serializer<PostList> get serializer => _$postListSerializer;
}