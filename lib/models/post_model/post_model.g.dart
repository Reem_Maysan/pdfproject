// GENERATED CODE - DO NOT MODIFY BY HAND

part of post_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<PostModel> _$postModelSerializer = new _$PostModelSerializer();

class _$PostModelSerializer implements StructuredSerializer<PostModel> {
  @override
  final Iterable<Type> types = const [PostModel, _$PostModel];
  @override
  final String wireName = 'PostModel';

  @override
  Iterable<Object> serialize(Serializers serializers, PostModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.data != null) {
      result
        ..add('data')
        ..add(serializers.serialize(object.data,
            specifiedType:
                const FullType(BuiltList, const [const FullType(PostList)])));
    }
    return result;
  }

  @override
  PostModel deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new PostModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'data':
          result.data.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(PostList)]))
              as BuiltList<Object>);
          break;
      }
    }

    return result.build();
  }
}

class _$PostModel extends PostModel {
  @override
  final BuiltList<PostList> data;

  factory _$PostModel([void Function(PostModelBuilder) updates]) =>
      (new PostModelBuilder()..update(updates)).build();

  _$PostModel._({this.data}) : super._();

  @override
  PostModel rebuild(void Function(PostModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PostModelBuilder toBuilder() => new PostModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PostModel && data == other.data;
  }

  @override
  int get hashCode {
    return $jf($jc(0, data.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PostModel')..add('data', data))
        .toString();
  }
}

class PostModelBuilder implements Builder<PostModel, PostModelBuilder> {
  _$PostModel _$v;

  ListBuilder<PostList> _data;
  ListBuilder<PostList> get data =>
      _$this._data ??= new ListBuilder<PostList>();
  set data(ListBuilder<PostList> data) => _$this._data = data;

  PostModelBuilder();

  PostModelBuilder get _$this {
    if (_$v != null) {
      _data = _$v.data?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PostModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$PostModel;
  }

  @override
  void update(void Function(PostModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$PostModel build() {
    _$PostModel _$result;
    try {
      _$result = _$v ?? new _$PostModel._(data: _data?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'data';
        _data?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'PostModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
