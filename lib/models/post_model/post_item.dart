import 'package:pdfproject/models/user_model/user_model.dart';

class PostItem {
  int id;
  String title;
  String description;
  String baseImage;
  String coverImage;
  String user_id;
  String created_at;
  String updated_at;
  UserModel user;

  PostItem(
      {this.id,
      this.title,
      this.description,
      this.baseImage,
      this.coverImage,
      this.user_id,
      this.created_at,
      this.updated_at,
      this.user});
}
