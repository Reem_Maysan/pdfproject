library post_model;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:pdfproject/models/post_model/post_list.dart';
import 'package:pdfproject/models/serializer/serializer.dart';

part 'post_model.g.dart';

abstract class PostModel implements Built<PostModel, PostModelBuilder> {

  PostModel._();

  @nullable
  @BuiltValueField(wireName: "data")
  BuiltList<PostList> get data;


  factory PostModel([updates(PostModelBuilder b)]) = _$PostModel;

  String toJson() {
    return json.encode(serializers.serializeWith(PostModel.serializer, this));
  }

  static PostModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        PostModel.serializer, json.decode(jsonString));
  }

  static Serializer<PostModel> get serializer => _$postModelSerializer;
}
