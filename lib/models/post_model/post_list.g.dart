// GENERATED CODE - DO NOT MODIFY BY HAND

part of post_list;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<PostList> _$postListSerializer = new _$PostListSerializer();

class _$PostListSerializer implements StructuredSerializer<PostList> {
  @override
  final Iterable<Type> types = const [PostList, _$PostList];
  @override
  final String wireName = 'PostList';

  @override
  Iterable<Object> serialize(Serializers serializers, PostList object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
      'title',
      serializers.serialize(object.title,
          specifiedType: const FullType(String)),
      'description',
      serializers.serialize(object.description,
          specifiedType: const FullType(String)),
      'image',
      serializers.serialize(object.image,
          specifiedType: const FullType(String)),
      'user_id',
      serializers.serialize(object.user_id,
          specifiedType: const FullType(String)),
      'created_at',
      serializers.serialize(object.created_at,
          specifiedType: const FullType(String)),
      'updated_at',
      serializers.serialize(object.updated_at,
          specifiedType: const FullType(String)),
      'user',
      serializers.serialize(object.user,
          specifiedType: const FullType(UserModel)),
    ];

    return result;
  }

  @override
  PostList deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new PostListBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'image':
          result.image = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'user_id':
          result.user_id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'created_at':
          result.created_at = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'updated_at':
          result.updated_at = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'user':
          result.user.replace(serializers.deserialize(value,
              specifiedType: const FullType(UserModel)) as UserModel);
          break;
      }
    }

    return result.build();
  }
}

class _$PostList extends PostList {
  @override
  final int id;
  @override
  final String title;
  @override
  final String description;
  @override
  final String image;
  @override
  final String user_id;
  @override
  final String created_at;
  @override
  final String updated_at;
  @override
  final UserModel user;

  factory _$PostList([void Function(PostListBuilder) updates]) =>
      (new PostListBuilder()..update(updates)).build();

  _$PostList._(
      {this.id,
      this.title,
      this.description,
      this.image,
      this.user_id,
      this.created_at,
      this.updated_at,
      this.user})
      : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('PostList', 'id');
    }
    if (title == null) {
      throw new BuiltValueNullFieldError('PostList', 'title');
    }
    if (description == null) {
      throw new BuiltValueNullFieldError('PostList', 'description');
    }
    if (image == null) {
      throw new BuiltValueNullFieldError('PostList', 'image');
    }
    if (user_id == null) {
      throw new BuiltValueNullFieldError('PostList', 'user_id');
    }
    if (created_at == null) {
      throw new BuiltValueNullFieldError('PostList', 'created_at');
    }
    if (updated_at == null) {
      throw new BuiltValueNullFieldError('PostList', 'updated_at');
    }
    if (user == null) {
      throw new BuiltValueNullFieldError('PostList', 'user');
    }
  }

  @override
  PostList rebuild(void Function(PostListBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PostListBuilder toBuilder() => new PostListBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PostList &&
        id == other.id &&
        title == other.title &&
        description == other.description &&
        image == other.image &&
        user_id == other.user_id &&
        created_at == other.created_at &&
        updated_at == other.updated_at &&
        user == other.user;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc($jc($jc(0, id.hashCode), title.hashCode),
                            description.hashCode),
                        image.hashCode),
                    user_id.hashCode),
                created_at.hashCode),
            updated_at.hashCode),
        user.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PostList')
          ..add('id', id)
          ..add('title', title)
          ..add('description', description)
          ..add('image', image)
          ..add('user_id', user_id)
          ..add('created_at', created_at)
          ..add('updated_at', updated_at)
          ..add('user', user))
        .toString();
  }
}

class PostListBuilder implements Builder<PostList, PostListBuilder> {
  _$PostList _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _title;
  String get title => _$this._title;
  set title(String title) => _$this._title = title;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  String _image;
  String get image => _$this._image;
  set image(String image) => _$this._image = image;

  String _user_id;
  String get user_id => _$this._user_id;
  set user_id(String user_id) => _$this._user_id = user_id;

  String _created_at;
  String get created_at => _$this._created_at;
  set created_at(String created_at) => _$this._created_at = created_at;

  String _updated_at;
  String get updated_at => _$this._updated_at;
  set updated_at(String updated_at) => _$this._updated_at = updated_at;

  UserModelBuilder _user;
  UserModelBuilder get user => _$this._user ??= new UserModelBuilder();
  set user(UserModelBuilder user) => _$this._user = user;

  PostListBuilder();

  PostListBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _title = _$v.title;
      _description = _$v.description;
      _image = _$v.image;
      _user_id = _$v.user_id;
      _created_at = _$v.created_at;
      _updated_at = _$v.updated_at;
      _user = _$v.user?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PostList other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$PostList;
  }

  @override
  void update(void Function(PostListBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$PostList build() {
    _$PostList _$result;
    try {
      _$result = _$v ??
          new _$PostList._(
              id: id,
              title: title,
              description: description,
              image: image,
              user_id: user_id,
              created_at: created_at,
              updated_at: updated_at,
              user: user.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'user';
        user.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'PostList', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
