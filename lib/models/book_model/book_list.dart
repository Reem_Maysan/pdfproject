library book_list;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:pdfproject/models/serializer/serializer.dart';

part 'book_list.g.dart';

abstract class BookList implements Built<BookList, BookListBuilder> {
  // fields go here

  int get id;
  String get title;
  String get name;
  String get author;
  String get publisher;
  String get date;
  String get file;
  String get created_at;
  String get updated_at;

  @nullable
  bool get isExpanded;

  BookList._();

  factory BookList([updates(BookListBuilder b)]) = _$BookList;

  String toJson() {
    return json.encode(serializers.serializeWith(BookList.serializer, this));
  }

  static BookList fromJson(String jsonString) {
    return serializers.deserializeWith(BookList.serializer, json.decode(jsonString));
  }

  static Serializer<BookList> get serializer => _$bookListSerializer;
}