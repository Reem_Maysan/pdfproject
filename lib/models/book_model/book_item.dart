class BookItem {
  String headerValue;
  String expandedValue;
  bool isExpanded;
  int id;
  String title;
  String name;
  String author;
  String publisher;
  String date;
  String file;
  String created_at;
  String updated_at;

  BookItem({
    this.headerValue,
    this.expandedValue,
    this.isExpanded = false,
    this.id,
    this.title,
    this.name,
    this.author,
    this.publisher,
    this.date,
    this.file,
    this.created_at,
    this.updated_at,
  });
}
