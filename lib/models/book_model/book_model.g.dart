// GENERATED CODE - DO NOT MODIFY BY HAND

part of book_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<BookModel> _$bookModelSerializer = new _$BookModelSerializer();

class _$BookModelSerializer implements StructuredSerializer<BookModel> {
  @override
  final Iterable<Type> types = const [BookModel, _$BookModel];
  @override
  final String wireName = 'BookModel';

  @override
  Iterable<Object> serialize(Serializers serializers, BookModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.books != null) {
      result
        ..add('data')
        ..add(serializers.serialize(object.books,
            specifiedType:
                const FullType(BuiltList, const [const FullType(BookList)])));
    }
    return result;
  }

  @override
  BookModel deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new BookModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'data':
          result.books.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(BookList)]))
              as BuiltList<Object>);
          break;
      }
    }

    return result.build();
  }
}

class _$BookModel extends BookModel {
  @override
  final BuiltList<BookList> books;

  factory _$BookModel([void Function(BookModelBuilder) updates]) =>
      (new BookModelBuilder()..update(updates)).build();

  _$BookModel._({this.books}) : super._();

  @override
  BookModel rebuild(void Function(BookModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BookModelBuilder toBuilder() => new BookModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BookModel && books == other.books;
  }

  @override
  int get hashCode {
    return $jf($jc(0, books.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('BookModel')..add('books', books))
        .toString();
  }
}

class BookModelBuilder implements Builder<BookModel, BookModelBuilder> {
  _$BookModel _$v;

  ListBuilder<BookList> _books;
  ListBuilder<BookList> get books =>
      _$this._books ??= new ListBuilder<BookList>();
  set books(ListBuilder<BookList> books) => _$this._books = books;

  BookModelBuilder();

  BookModelBuilder get _$this {
    if (_$v != null) {
      _books = _$v.books?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(BookModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$BookModel;
  }

  @override
  void update(void Function(BookModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$BookModel build() {
    _$BookModel _$result;
    try {
      _$result = _$v ?? new _$BookModel._(books: _books?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'books';
        _books?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'BookModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
