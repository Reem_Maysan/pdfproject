library book_model;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:pdfproject/models/book_model/book_list.dart';
import 'package:pdfproject/models/serializer/serializer.dart';

part 'book_model.g.dart';

abstract class BookModel implements Built<BookModel, BookModelBuilder> {
  // fields go here

  BookModel._();

  @nullable
  @BuiltValueField(wireName: "data")
  BuiltList<BookList> get books;


  factory BookModel([updates(BookModelBuilder b)]) = _$BookModel;

  String toJson() {
    return json.encode(serializers.serializeWith(BookModel.serializer, this));
  }

  static BookModel fromJson(String jsonString) {
    return serializers.deserializeWith(BookModel.serializer, json.decode(jsonString));
  }

  static Serializer<BookModel> get serializer => _$bookModelSerializer;
}