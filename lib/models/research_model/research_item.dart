class ResearchItem {
  int id;
  String title;
  String author;
  String company;
  String date;
  String file;
  String created_at;
  String updated_at;

  ResearchItem({
    this.id,
    this.title,
    this.author,
    this.company,
    this.date,
    this.file,
    this.created_at,
    this.updated_at,
  });
}
