library research_list;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:pdfproject/models/serializer/serializer.dart';

part 'research_list.g.dart';

abstract class ResearchList
    implements Built<ResearchList, ResearchListBuilder> {

  ResearchList._();

  int get id;
  String get title;
  String get author;
  String get company;
  String get date;
  String get file;
  String get created_at;
  String get updated_at;

  @nullable
  bool get isExpanded;

  factory ResearchList([updates(ResearchListBuilder b)]) = _$ResearchList;

  String toJson() {
    return json
        .encode(serializers.serializeWith(ResearchList.serializer, this));
  }

  static ResearchList fromJson(String jsonString) {
    return serializers.deserializeWith(
        ResearchList.serializer, json.decode(jsonString));
  }

  static Serializer<ResearchList> get serializer => _$researchListSerializer;
}
