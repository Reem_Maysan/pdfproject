library research_model;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:pdfproject/models/research_model/research_list.dart';
import 'package:pdfproject/models/serializer/serializer.dart';

part 'research_model.g.dart';

abstract class ResearchModel
    implements Built<ResearchModel, ResearchModelBuilder> {
  // fields go here

  ResearchModel._();

  @nullable
  @BuiltValueField(wireName: "data")
  BuiltList<ResearchList> get researches;

  factory ResearchModel([updates(ResearchModelBuilder b)]) = _$ResearchModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(ResearchModel.serializer, this));
  }

  static ResearchModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        ResearchModel.serializer, json.decode(jsonString));
  }

  static Serializer<ResearchModel> get serializer => _$researchModelSerializer;
}
