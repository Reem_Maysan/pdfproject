// GENERATED CODE - DO NOT MODIFY BY HAND

part of research_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ResearchModel> _$researchModelSerializer =
    new _$ResearchModelSerializer();

class _$ResearchModelSerializer implements StructuredSerializer<ResearchModel> {
  @override
  final Iterable<Type> types = const [ResearchModel, _$ResearchModel];
  @override
  final String wireName = 'ResearchModel';

  @override
  Iterable<Object> serialize(Serializers serializers, ResearchModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.researches != null) {
      result
        ..add('data')
        ..add(serializers.serialize(object.researches,
            specifiedType: const FullType(
                BuiltList, const [const FullType(ResearchList)])));
    }
    return result;
  }

  @override
  ResearchModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ResearchModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'data':
          result.researches.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(ResearchList)]))
              as BuiltList<Object>);
          break;
      }
    }

    return result.build();
  }
}

class _$ResearchModel extends ResearchModel {
  @override
  final BuiltList<ResearchList> researches;

  factory _$ResearchModel([void Function(ResearchModelBuilder) updates]) =>
      (new ResearchModelBuilder()..update(updates)).build();

  _$ResearchModel._({this.researches}) : super._();

  @override
  ResearchModel rebuild(void Function(ResearchModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ResearchModelBuilder toBuilder() => new ResearchModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ResearchModel && researches == other.researches;
  }

  @override
  int get hashCode {
    return $jf($jc(0, researches.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ResearchModel')
          ..add('researches', researches))
        .toString();
  }
}

class ResearchModelBuilder
    implements Builder<ResearchModel, ResearchModelBuilder> {
  _$ResearchModel _$v;

  ListBuilder<ResearchList> _researches;
  ListBuilder<ResearchList> get researches =>
      _$this._researches ??= new ListBuilder<ResearchList>();
  set researches(ListBuilder<ResearchList> researches) =>
      _$this._researches = researches;

  ResearchModelBuilder();

  ResearchModelBuilder get _$this {
    if (_$v != null) {
      _researches = _$v.researches?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ResearchModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ResearchModel;
  }

  @override
  void update(void Function(ResearchModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ResearchModel build() {
    _$ResearchModel _$result;
    try {
      _$result = _$v ?? new _$ResearchModel._(researches: _researches?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'researches';
        _researches?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ResearchModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
