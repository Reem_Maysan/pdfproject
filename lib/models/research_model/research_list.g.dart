// GENERATED CODE - DO NOT MODIFY BY HAND

part of research_list;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ResearchList> _$researchListSerializer =
    new _$ResearchListSerializer();

class _$ResearchListSerializer implements StructuredSerializer<ResearchList> {
  @override
  final Iterable<Type> types = const [ResearchList, _$ResearchList];
  @override
  final String wireName = 'ResearchList';

  @override
  Iterable<Object> serialize(Serializers serializers, ResearchList object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
      'title',
      serializers.serialize(object.title,
          specifiedType: const FullType(String)),
      'author',
      serializers.serialize(object.author,
          specifiedType: const FullType(String)),
      'company',
      serializers.serialize(object.company,
          specifiedType: const FullType(String)),
      'date',
      serializers.serialize(object.date, specifiedType: const FullType(String)),
      'file',
      serializers.serialize(object.file, specifiedType: const FullType(String)),
      'created_at',
      serializers.serialize(object.created_at,
          specifiedType: const FullType(String)),
      'updated_at',
      serializers.serialize(object.updated_at,
          specifiedType: const FullType(String)),
    ];
    if (object.isExpanded != null) {
      result
        ..add('isExpanded')
        ..add(serializers.serialize(object.isExpanded,
            specifiedType: const FullType(bool)));
    }
    return result;
  }

  @override
  ResearchList deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ResearchListBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'author':
          result.author = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'company':
          result.company = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'date':
          result.date = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'file':
          result.file = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'created_at':
          result.created_at = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'updated_at':
          result.updated_at = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'isExpanded':
          result.isExpanded = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
      }
    }

    return result.build();
  }
}

class _$ResearchList extends ResearchList {
  @override
  final int id;
  @override
  final String title;
  @override
  final String author;
  @override
  final String company;
  @override
  final String date;
  @override
  final String file;
  @override
  final String created_at;
  @override
  final String updated_at;
  @override
  final bool isExpanded;

  factory _$ResearchList([void Function(ResearchListBuilder) updates]) =>
      (new ResearchListBuilder()..update(updates)).build();

  _$ResearchList._(
      {this.id,
      this.title,
      this.author,
      this.company,
      this.date,
      this.file,
      this.created_at,
      this.updated_at,
      this.isExpanded})
      : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('ResearchList', 'id');
    }
    if (title == null) {
      throw new BuiltValueNullFieldError('ResearchList', 'title');
    }
    if (author == null) {
      throw new BuiltValueNullFieldError('ResearchList', 'author');
    }
    if (company == null) {
      throw new BuiltValueNullFieldError('ResearchList', 'company');
    }
    if (date == null) {
      throw new BuiltValueNullFieldError('ResearchList', 'date');
    }
    if (file == null) {
      throw new BuiltValueNullFieldError('ResearchList', 'file');
    }
    if (created_at == null) {
      throw new BuiltValueNullFieldError('ResearchList', 'created_at');
    }
    if (updated_at == null) {
      throw new BuiltValueNullFieldError('ResearchList', 'updated_at');
    }
  }

  @override
  ResearchList rebuild(void Function(ResearchListBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ResearchListBuilder toBuilder() => new ResearchListBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ResearchList &&
        id == other.id &&
        title == other.title &&
        author == other.author &&
        company == other.company &&
        date == other.date &&
        file == other.file &&
        created_at == other.created_at &&
        updated_at == other.updated_at &&
        isExpanded == other.isExpanded;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc($jc($jc(0, id.hashCode), title.hashCode),
                                author.hashCode),
                            company.hashCode),
                        date.hashCode),
                    file.hashCode),
                created_at.hashCode),
            updated_at.hashCode),
        isExpanded.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ResearchList')
          ..add('id', id)
          ..add('title', title)
          ..add('author', author)
          ..add('company', company)
          ..add('date', date)
          ..add('file', file)
          ..add('created_at', created_at)
          ..add('updated_at', updated_at)
          ..add('isExpanded', isExpanded))
        .toString();
  }
}

class ResearchListBuilder
    implements Builder<ResearchList, ResearchListBuilder> {
  _$ResearchList _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _title;
  String get title => _$this._title;
  set title(String title) => _$this._title = title;

  String _author;
  String get author => _$this._author;
  set author(String author) => _$this._author = author;

  String _company;
  String get company => _$this._company;
  set company(String company) => _$this._company = company;

  String _date;
  String get date => _$this._date;
  set date(String date) => _$this._date = date;

  String _file;
  String get file => _$this._file;
  set file(String file) => _$this._file = file;

  String _created_at;
  String get created_at => _$this._created_at;
  set created_at(String created_at) => _$this._created_at = created_at;

  String _updated_at;
  String get updated_at => _$this._updated_at;
  set updated_at(String updated_at) => _$this._updated_at = updated_at;

  bool _isExpanded;
  bool get isExpanded => _$this._isExpanded;
  set isExpanded(bool isExpanded) => _$this._isExpanded = isExpanded;

  ResearchListBuilder();

  ResearchListBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _title = _$v.title;
      _author = _$v.author;
      _company = _$v.company;
      _date = _$v.date;
      _file = _$v.file;
      _created_at = _$v.created_at;
      _updated_at = _$v.updated_at;
      _isExpanded = _$v.isExpanded;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ResearchList other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ResearchList;
  }

  @override
  void update(void Function(ResearchListBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ResearchList build() {
    final _$result = _$v ??
        new _$ResearchList._(
            id: id,
            title: title,
            author: author,
            company: company,
            date: date,
            file: file,
            created_at: created_at,
            updated_at: updated_at,
            isExpanded: isExpanded);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
