// GENERATED CODE - DO NOT MODIFY BY HAND

part of publication_model;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<PublicationModel> _$publicationModelSerializer =
    new _$PublicationModelSerializer();

class _$PublicationModelSerializer
    implements StructuredSerializer<PublicationModel> {
  @override
  final Iterable<Type> types = const [PublicationModel, _$PublicationModel];
  @override
  final String wireName = 'PublicationModel';

  @override
  Iterable<Object> serialize(Serializers serializers, PublicationModel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.publications != null) {
      result
        ..add('data')
        ..add(serializers.serialize(object.publications,
            specifiedType: const FullType(
                BuiltList, const [const FullType(PublicationList)])));
    }
    return result;
  }

  @override
  PublicationModel deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new PublicationModelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'data':
          result.publications.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(PublicationList)]))
              as BuiltList<Object>);
          break;
      }
    }

    return result.build();
  }
}

class _$PublicationModel extends PublicationModel {
  @override
  final BuiltList<PublicationList> publications;

  factory _$PublicationModel(
          [void Function(PublicationModelBuilder) updates]) =>
      (new PublicationModelBuilder()..update(updates)).build();

  _$PublicationModel._({this.publications}) : super._();

  @override
  PublicationModel rebuild(void Function(PublicationModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PublicationModelBuilder toBuilder() =>
      new PublicationModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PublicationModel && publications == other.publications;
  }

  @override
  int get hashCode {
    return $jf($jc(0, publications.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PublicationModel')
          ..add('publications', publications))
        .toString();
  }
}

class PublicationModelBuilder
    implements Builder<PublicationModel, PublicationModelBuilder> {
  _$PublicationModel _$v;

  ListBuilder<PublicationList> _publications;
  ListBuilder<PublicationList> get publications =>
      _$this._publications ??= new ListBuilder<PublicationList>();
  set publications(ListBuilder<PublicationList> publications) =>
      _$this._publications = publications;

  PublicationModelBuilder();

  PublicationModelBuilder get _$this {
    if (_$v != null) {
      _publications = _$v.publications?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PublicationModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$PublicationModel;
  }

  @override
  void update(void Function(PublicationModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$PublicationModel build() {
    _$PublicationModel _$result;
    try {
      _$result =
          _$v ?? new _$PublicationModel._(publications: _publications?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'publications';
        _publications?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'PublicationModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
