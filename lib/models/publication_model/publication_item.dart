class PublicationItem {
  int id;
  String type;
  String name;
  String year;
  String file;
  String created_at;
  String updated_at;

  PublicationItem({
    this.id,
    this.type,
    this.name,
    this.year,
    this.file,
    this.created_at,
    this.updated_at,
  });
}
