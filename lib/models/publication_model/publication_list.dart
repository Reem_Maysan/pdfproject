library publication_list;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:pdfproject/models/serializer/serializer.dart';

part 'publication_list.g.dart';

abstract class PublicationList
    implements Built<PublicationList, PublicationListBuilder> {
  // fields go here

  PublicationList._();

  int get id;
  String get name;
  String get file;
  String get type;
  String get year;
  String get created_at;
  String get updated_at;

  @nullable
  bool get isExpanded;

  factory PublicationList([updates(PublicationListBuilder b)]) =
      _$PublicationList;

  String toJson() {
    return json
        .encode(serializers.serializeWith(PublicationList.serializer, this));
  }

  static PublicationList fromJson(String jsonString) {
    return serializers.deserializeWith(
        PublicationList.serializer, json.decode(jsonString));
  }

  static Serializer<PublicationList> get serializer =>
      _$publicationListSerializer;
}
