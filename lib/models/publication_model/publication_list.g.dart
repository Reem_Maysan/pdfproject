// GENERATED CODE - DO NOT MODIFY BY HAND

part of publication_list;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<PublicationList> _$publicationListSerializer =
    new _$PublicationListSerializer();

class _$PublicationListSerializer
    implements StructuredSerializer<PublicationList> {
  @override
  final Iterable<Type> types = const [PublicationList, _$PublicationList];
  @override
  final String wireName = 'PublicationList';

  @override
  Iterable<Object> serialize(Serializers serializers, PublicationList object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'file',
      serializers.serialize(object.file, specifiedType: const FullType(String)),
      'type',
      serializers.serialize(object.type, specifiedType: const FullType(String)),
      'year',
      serializers.serialize(object.year, specifiedType: const FullType(String)),
      'created_at',
      serializers.serialize(object.created_at,
          specifiedType: const FullType(String)),
      'updated_at',
      serializers.serialize(object.updated_at,
          specifiedType: const FullType(String)),
    ];
    if (object.isExpanded != null) {
      result
        ..add('isExpanded')
        ..add(serializers.serialize(object.isExpanded,
            specifiedType: const FullType(bool)));
    }
    return result;
  }

  @override
  PublicationList deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new PublicationListBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'file':
          result.file = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'type':
          result.type = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'year':
          result.year = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'created_at':
          result.created_at = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'updated_at':
          result.updated_at = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'isExpanded':
          result.isExpanded = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
      }
    }

    return result.build();
  }
}

class _$PublicationList extends PublicationList {
  @override
  final int id;
  @override
  final String name;
  @override
  final String file;
  @override
  final String type;
  @override
  final String year;
  @override
  final String created_at;
  @override
  final String updated_at;
  @override
  final bool isExpanded;

  factory _$PublicationList([void Function(PublicationListBuilder) updates]) =>
      (new PublicationListBuilder()..update(updates)).build();

  _$PublicationList._(
      {this.id,
      this.name,
      this.file,
      this.type,
      this.year,
      this.created_at,
      this.updated_at,
      this.isExpanded})
      : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('PublicationList', 'id');
    }
    if (name == null) {
      throw new BuiltValueNullFieldError('PublicationList', 'name');
    }
    if (file == null) {
      throw new BuiltValueNullFieldError('PublicationList', 'file');
    }
    if (type == null) {
      throw new BuiltValueNullFieldError('PublicationList', 'type');
    }
    if (year == null) {
      throw new BuiltValueNullFieldError('PublicationList', 'year');
    }
    if (created_at == null) {
      throw new BuiltValueNullFieldError('PublicationList', 'created_at');
    }
    if (updated_at == null) {
      throw new BuiltValueNullFieldError('PublicationList', 'updated_at');
    }
  }

  @override
  PublicationList rebuild(void Function(PublicationListBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PublicationListBuilder toBuilder() =>
      new PublicationListBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PublicationList &&
        id == other.id &&
        name == other.name &&
        file == other.file &&
        type == other.type &&
        year == other.year &&
        created_at == other.created_at &&
        updated_at == other.updated_at &&
        isExpanded == other.isExpanded;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc($jc($jc(0, id.hashCode), name.hashCode),
                            file.hashCode),
                        type.hashCode),
                    year.hashCode),
                created_at.hashCode),
            updated_at.hashCode),
        isExpanded.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PublicationList')
          ..add('id', id)
          ..add('name', name)
          ..add('file', file)
          ..add('type', type)
          ..add('year', year)
          ..add('created_at', created_at)
          ..add('updated_at', updated_at)
          ..add('isExpanded', isExpanded))
        .toString();
  }
}

class PublicationListBuilder
    implements Builder<PublicationList, PublicationListBuilder> {
  _$PublicationList _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _file;
  String get file => _$this._file;
  set file(String file) => _$this._file = file;

  String _type;
  String get type => _$this._type;
  set type(String type) => _$this._type = type;

  String _year;
  String get year => _$this._year;
  set year(String year) => _$this._year = year;

  String _created_at;
  String get created_at => _$this._created_at;
  set created_at(String created_at) => _$this._created_at = created_at;

  String _updated_at;
  String get updated_at => _$this._updated_at;
  set updated_at(String updated_at) => _$this._updated_at = updated_at;

  bool _isExpanded;
  bool get isExpanded => _$this._isExpanded;
  set isExpanded(bool isExpanded) => _$this._isExpanded = isExpanded;

  PublicationListBuilder();

  PublicationListBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _name = _$v.name;
      _file = _$v.file;
      _type = _$v.type;
      _year = _$v.year;
      _created_at = _$v.created_at;
      _updated_at = _$v.updated_at;
      _isExpanded = _$v.isExpanded;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PublicationList other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$PublicationList;
  }

  @override
  void update(void Function(PublicationListBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$PublicationList build() {
    final _$result = _$v ??
        new _$PublicationList._(
            id: id,
            name: name,
            file: file,
            type: type,
            year: year,
            created_at: created_at,
            updated_at: updated_at,
            isExpanded: isExpanded);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
