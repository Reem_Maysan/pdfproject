library publication_model;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:pdfproject/models/publication_model/publication_list.dart';
import 'package:pdfproject/models/serializer/serializer.dart';

part 'publication_model.g.dart';

abstract class PublicationModel
    implements Built<PublicationModel, PublicationModelBuilder> {
  // fields go here

  PublicationModel._();

  @nullable
  @BuiltValueField(wireName: "data")
  BuiltList<PublicationList> get publications;

  factory PublicationModel([updates(PublicationModelBuilder b)]) =
      _$PublicationModel;

  String toJson() {
    return json
        .encode(serializers.serializeWith(PublicationModel.serializer, this));
  }

  static PublicationModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        PublicationModel.serializer, json.decode(jsonString));
  }

  static Serializer<PublicationModel> get serializer =>
      _$publicationModelSerializer;
}
