import 'dart:convert';
import 'dart:async';
import 'dart:io';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdfproject/constents/constent.dart';
import 'package:pdfproject/models/book_model/book_model.dart';
import 'package:pdfproject/models/post_model/post_list.dart';
import 'package:pdfproject/models/post_model/post_model.dart';
import 'package:pdfproject/models/publication_model/publication_model.dart';
import 'package:pdfproject/models/research_model/research_model.dart';
import 'Ihttp_helper.dart';
import 'package:dio/dio.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:built_value/serializer.dart';
import 'package:pdfproject/models/serializer/serializer.dart';
import 'package:cookie_jar/cookie_jar.dart';

class HttpHelper implements IHttpHelper {
  BaseOptions options = new BaseOptions(
    connectTimeout: 30000,
    receiveTimeout: 30000,
  );
  final Dio _dio;

  var cookieJar = CookieJar();

  HttpHelper(
    this._dio,
  ) {
    _dio.interceptors.add(
      LogInterceptor(
        request: true,
        responseBody: true,
        requestBody: true,
      ),
    );
    _dio.interceptors.add(CookieManager(cookieJar));
    // _dio.options = options;
  }

  @override
  Future<PostModel> getPost(int startId, int count) async {
    _dio.interceptors.add(CookieManager(cookieJar));
//?start_id=$startId&count=$count

    try {
      final response = await _dio
          .get(BaseUrl + "getArticles?start_id=$startId&count=$count");

      if (response.statusCode == 200) {
        print('getPost Response body  ${response.data}');

        final PostModel baseResponse = serializers.deserialize(
            json.decode(response.data),
            specifiedType: FullType(PostModel));

        if (baseResponse != null) {
          return baseResponse;
        } else {
          throw Exception();
        }
      } else {
        throw Exception();
      }
    } catch (e) {
      print(e.toString());
      throw Exception();
    }
  }

  @override
  Future<BookModel> getBook(int startId, int count) async {
    _dio.interceptors.add(CookieManager(cookieJar));
    try {
      final response = await _dio.get(
          BaseUrl + "getBooks?type=read_book&start_id=$startId&count=$count");

      if (response.statusCode == 200) {
        print('getBook Response body  ${response.data}');

        final BookModel baseResponse = serializers.deserialize(
            json.decode(response.data),
            specifiedType: FullType(BookModel));

        print("getBook status : $baseResponse");
        if (baseResponse != null) {
          return baseResponse;
        } else {
          throw Exception();
        }
      } else {
        throw Exception();
      }
    } catch (e) {
      print(e.toString());
      throw Exception();
    }
  }

  @override
  Future<PublicationModel> getPublicationEbook(int startId, int count) async {
    _dio.interceptors.add(CookieManager(cookieJar));
    try {
      final response = await _dio
          .get(BaseUrl + "getBooks?type=e_book&start_id=$startId&count=$count");

      if (response.statusCode == 200) {
        print('getPublicationEbook Response body  ${response.data}');

        final PublicationModel baseResponse = serializers.deserialize(
            json.decode(response.data),
            specifiedType: FullType(PublicationModel));

        print("getPublicationEbook status : $baseResponse");
        if (baseResponse != null) {
          return baseResponse;
        } else {
          throw Exception();
        }
      } else {
        throw Exception();
      }
    } catch (e) {
      print(e.toString());
      throw Exception();
    }
  }

  @override
  Future<PublicationModel> getPublicationEmagazine(
      int startId, int count) async {
    _dio.interceptors.add(CookieManager(cookieJar));
    try {
      final response = await _dio.get(
          BaseUrl + "getBooks?type=e_magazine&start_id=$startId&count=$count");

      if (response.statusCode == 200) {
        print('getPublicationEmagazine Response body  ${response.data}');

        final PublicationModel baseResponse = serializers.deserialize(
            json.decode(response.data),
            specifiedType: FullType(PublicationModel));

        print("getPublicationEmagazine status : $baseResponse");
        if (baseResponse != null) {
          return baseResponse;
        } else {
          throw Exception();
        }
      } else {
        throw Exception();
      }
    } catch (e) {
      print(e.toString());
      throw Exception();
    }
  }

  @override
  Future<PublicationModel> getPublicationEpublisher(
      int startId, int count) async {
    _dio.interceptors.add(CookieManager(cookieJar));
    try {
      final response = await _dio.get(
          BaseUrl + "getBooks?type=e_publisher&start_id=$startId&count=$count");

      if (response.statusCode == 200) {
        print('getPublicationEpublisher Response body  ${response.data}');

        final PublicationModel baseResponse = serializers.deserialize(
            json.decode(response.data),
            specifiedType: FullType(PublicationModel));

        print("getPublicationEpublisher status : $baseResponse");
        if (baseResponse != null) {
          return baseResponse;
        } else {
          throw Exception();
        }
      } else {
        throw Exception();
      }
    } catch (e) {
      print(e.toString());
      throw Exception();
    }
  }

  @override
  Future getPdf(String url) async {
    BaseOptions options = new BaseOptions(
      connectTimeout: 120000,
      receiveTimeout: 120000,
    );
    Dio dio2 = new Dio(options);
    _dio.interceptors.add(CookieManager(cookieJar));
    String pdfName = '_file.pdf';

    Directory documents;
    if (Platform.isAndroid) {
      documents = await getExternalStorageDirectory();
    } else {
      documents = await getApplicationDocumentsDirectory();
    }

    String savePath = documents.path + '/$pdfName';
    try {
      await dio2.download(url, savePath, onReceiveProgress: (rec, total) {
        print('Rec: $rec Total: $total');

        if (rec == total) {
          File file = File(savePath);
          OpenFile.open(file.path);
        }
      });
    } on DioError catch (e) {
      print('DioError: ' + e.toString());
    }
  }

  @override
  Future<ResearchModel> getResearches(int startId, int count) async {
    _dio.interceptors.add(CookieManager(cookieJar));
    try {
      final response = await _dio.get(
          BaseUrl + "getBooks?type=research&start_id=$startId&count=$count");

      if (response.statusCode == 200) {
        print('getResearches Response body  ${response.data}');

        final ResearchModel baseResponse = serializers.deserialize(
            json.decode(response.data),
            specifiedType: FullType(ResearchModel));

        print("getResearches status : $baseResponse");
        if (baseResponse != null) {
          return baseResponse;
        } else {
          throw Exception();
        }
      } else {
        throw Exception();
      }
    } catch (e) {
      print(e.toString());
      throw Exception();
    }
  }

  @override
  Future<PostList> getPostById(int articleId) async {
    _dio.interceptors.add(CookieManager(cookieJar));
    try {
      final response =
          await _dio.get(BaseUrl + "getArticleDetailes?article_id=$articleId");

      if (response.statusCode == 200) {
        print('getPostById Response body  ${response.data}');

        final PostList baseResponse = serializers.deserialize(
            json.decode(response.data),
            specifiedType: FullType(PostList));

        print("getPostById status : $baseResponse");
        if (baseResponse != null) {
          return baseResponse;
        } else {
          throw Exception();
        }
      } else {
        throw Exception();
      }
    } catch (e) {
      print(e.toString());
      throw Exception();
    }
  }
}
