import 'dart:io';
import 'dart:math';

import 'package:pdfproject/constents/constent.dart';
import 'package:pdfproject/data/http_helper/Ihttp_helper.dart';
import 'package:pdfproject/models/book_model/book_model.dart';
import 'package:pdfproject/models/post_model/post_list.dart';
import 'package:pdfproject/models/post_model/post_model.dart';
import 'package:pdfproject/models/publication_model/publication_model.dart';
import 'package:pdfproject/models/research_model/research_model.dart';

import 'irepository.dart';

class Repository implements IRepository {
  IHttpHelper _ihttpHelper;

  Repository(this._ihttpHelper);

  @override
  Future<PostModel> getPost(int startId, int count) async {
    final data = await _ihttpHelper.getPost(startId, count);
    return data;
  }

  @override
  Future<BookModel> getBook(int startId, int count) async {
    final data = await _ihttpHelper.getBook(startId, count);
    return data;
  }

  @override
  Future<PublicationModel> getPublicationEbook(int startId, int count) async {
    final data = await _ihttpHelper.getPublicationEbook(startId, count);
    return data;
  }

  @override
  Future<PublicationModel> getPublicationEmagazine(
      int startId, int count) async {
    final data = await _ihttpHelper.getPublicationEmagazine(startId, count);
    return data;
  }

  @override
  Future<PublicationModel> getPublicationEpublisher(
      int startId, int count) async {
    final data = await _ihttpHelper.getPublicationEpublisher(startId, count);
    return data;
  }

  @override
  Future getPdf(String url) async {
    final data = await _ihttpHelper.getPdf(url);
    return data;
  }

  @override
  Future<ResearchModel> getResearches(int startId, int count) async {
    final data = await _ihttpHelper.getResearches(startId, count);
    return data;
  }

  @override
  Future<PostList> getPostById(int articleId) async {
    final data = await _ihttpHelper.getPostById(articleId);
    return data;
  }
}
