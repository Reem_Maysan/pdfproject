import 'dart:io';

import 'package:pdfproject/constents/constent.dart';
import 'package:pdfproject/models/book_model/book_model.dart';
import 'package:pdfproject/models/post_model/post_list.dart';
import 'package:pdfproject/models/post_model/post_model.dart';
import 'package:pdfproject/models/publication_model/publication_model.dart';
import 'package:pdfproject/models/research_model/research_model.dart';

abstract class IRepository {
  Future<PostModel> getPost(int startId,int count);
  Future<PostList> getPostById(int articleId);
  Future<BookModel> getBook(int startId, int count);
  Future<ResearchModel> getResearches(int startId, int count);
  Future<PublicationModel> getPublicationEbook(int startId, int count);
  Future<PublicationModel> getPublicationEpublisher(int startId, int count);
  Future<PublicationModel> getPublicationEmagazine(int startId, int count);
  Future getPdf(String url);
}
