import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:pdfproject/constents/constent.dart';
import 'package:pdfproject/ui/Enterprise_Publications_pages/bloc/publications_bloc.dart';
import 'package:pdfproject/ui/articles_page/bloc/articles_bloc.dart';
import 'package:pdfproject/ui/reading_book_page/bloc/reading_book_bloc.dart';
import 'package:pdfproject/ui/research_page/bloc/research_page_bloc.dart';

import 'data/http_helper/Ihttp_helper.dart';
import 'data/http_helper/http_helper.dart';
import 'data/repository/irepository.dart';
import 'data/repository/repository.dart';

final sl = GetIt.instance;

Future iniGetIt() async {
  sl.registerLazySingleton(() => ((Dio(BaseOptions(
      baseUrl: BaseUrl,
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        "charset": "utf-8",
        "Accept-Charset": "utf-8"
      },
      responseType: ResponseType.plain)))));

  // data
  
  sl.registerLazySingleton<PageParameters>(() => PageParameters());
  sl.registerLazySingleton<IHttpHelper>(() => HttpHelper(sl()));
  sl.registerLazySingleton<IRepository>(() => Repository(sl()));

  /// ArticlesBloc

  sl.registerFactory(() => ArticlesBloc(sl()));

  /// ReadingBookBloc

  sl.registerFactory(() => ReadingBookBloc(sl()));

  /// PublicationsBloc

  sl.registerFactory(() => PublicationsBloc(sl()));
  
  //ResearchBloc
  
  sl.registerFactory(() => ResearchBloc(sl()));
}
