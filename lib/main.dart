import 'package:flutter/material.dart';
import 'package:pdfproject/injectoin.dart';
import 'package:pdfproject/ui/welcome_page/welcome_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await iniGetIt();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: WelcomePage(launchUrl: false,),
    );
  }
}
